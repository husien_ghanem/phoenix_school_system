<?php

namespace App\Http\Controllers;

use App\Http\Controllers\general\UploadController;
use App\Models\Answers;
use App\Models\Question;
use App\WordParser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use PhpOffice\PhpWord\IOFactory;

class WordParserController extends Controller
{
    protected $uploadController;

    public function __construct(UploadController $uploadController)
    {
        $this->uploadController = $uploadController;
    }



    public function upload(Request $request)
    {


        // Validate the uploaded file
        $request->validate([
            'wordFile' => 'required|mimes:docx',
        ]);

        $uploadedFileName = $this->uploadController->upload_word($request);

        if ($uploadedFileName) {
            // If successful, parse the uploaded Word document
            $parsedData = $this->parseUploadedWordDocument($uploadedFileName);

            return redirect()->back()->with('success', 'File uploaded successfully.');
        } else {
            // Handle the case where the file upload failed
            return redirect()->back()->with('error', 'File upload failed. Please try again.');
        }
    }

    protected function parseUploadedWordDocument($fileName)
    {
        // Get the full path to the uploaded Word document
        $filePath = storage_path('app/public/media/word_files/' . $fileName);

        // Create an instance of the WordParser class
        $wordParser = new WordParser();

        // Parse the Word document to retrieve question data
        $parsedData = $wordParser->parseDocument($filePath);

        // Process the parsed data (e.g., save it to the database)
        foreach ($parsedData as $questionData) {
            // You can save the question data to your database here
            // Example:
            $question = new Question();
            $question->title = $questionData['title'];
            $question->description = $questionData['description'];
            $question->value = $questionData['value'];
            $question->save();

            foreach ($questionData['answer_choices'] as $choiceNumber => $choiceText) {
                $answer = new Answers();
                $answer->question_id = $question->id;
                $answer->answer_text = $choiceText;

                // Check if this choice is the correct answer
                if ($choiceNumber == $questionData['correct_answer']) {
                    $answer->is_correct = true;
                }

                $answer->save();
                DB::table('questions_answers')->insert([
                    'answer_id' => $answer->id,
                    'question_id' => $question->id,
                ]);

            }
        }

        return $parsedData;
    }


}


