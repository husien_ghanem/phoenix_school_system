<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Models\Answers;
use App\Models\Question;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AnswersController extends Controller
{
  public function addAnswers ($id)
  {
      $data = Question::findOrFail($id);
      $answers = Answers::where('question_id', $id)->get();
      return view('admin.answers.index', compact('data','answers'));
  }

    public function store(Request $request)
    {

        $request->validate([
            'question_id' => 'required',
            'answer_text' => 'required|string',
            'is_correct' => 'nullable',
        ]);

        $answer = new Answers([
            'question_id' => $request->question_id,
            'answer_text' => $request->input('answer_text'),
            'is_correct' => $request->input('is_correct'),
        ]);
        $answer->save();

        DB::table('questions_answers')->insert([
            'answer_id' => $answer->id,
            'question_id' => $request->question_id,
        ]);


        return redirect()->route('dashboard.questions.answers', $request->question_id)->with('success', 'Answer added successfully.');
    }
}
