<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Models\Classes;
use Illuminate\Http\Request;
use App\Http\Requests\ClassesRequest;

class ClassesController extends Controller
{
    public function index()
    {
        return view('admin.classes.index');
    }

    public function all_classes()
    {
        $data = Classes::latest()->get();
        return $data;
    }

    public function store(ClassesRequest $request)
    {
        $data = Classes::create($request->all());
        return response()->json(['success' => "true"], 200);
    }

    public function edit($id)
    {
        $data = Classes::find($id);
        return view('admin.classes.edit', compact('data'));
    }

    public function show($id)
    {
        $data = Classes::with('sections')->find($id);
        return view('admin.classes.show', compact('data'));
    }

    public function update(ClassesRequest $request, $id)
    {
        $data = Classes::find($id);
        $data->update($request->all());
        return response()->json(['success' => "true"], 200);
    }
}
