<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Models\AppUsers;

class GeneralController extends Controller
{
    public function dashboard_index(Request $request)
    {
        
        return view('admin.index');
    }

}