<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Models\Answers;
use App\Models\Test;
use Illuminate\Http\Request;
use App\Models\Question;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\general\UploadController;
class QuestionController extends Controller
{

    protected $uploadController;

    public function __construct(UploadController $uploadController)
    {
        $this->uploadController = $uploadController;
    }

    public function index()
    {
        $questions = Question::all();
        return view('admin.questions.index', ['questions' => $questions]);
    }

    public function all_questions_test($id)
    {
        $test = Test::findOrFail($id);
        $questions = Question::whereHas('tests', function ($query) use ($test) {
            $query->where('test_id', $test->id);
        })->get();

        return $questions;
    }
//    public function add_random_questions($id, Request $request)
//    {
//        $test = Test::findOrFail($id);
//        $availableQuestions = Question::all();
//        $randomQuestions = $availableQuestions->shuffle()->take($request->input('random_question_count'));
//        $test->questions()->attach($randomQuestions);
//        return redirect()->route('dashboard.tests.show', $id);
//
//    }
    public function add_random_questions($id, Request $request)
    {
        $test = Test::findOrFail($id);
        $availableQuestions = Question::all();

        // Get the currently attached question IDs for the test
        $attachedQuestionIds = $test->questions->pluck('id')->toArray();

        // Filter available questions to exclude those already attached
        $remainingQuestions = $availableQuestions->reject(function ($question) use ($attachedQuestionIds) {
            return in_array($question->id, $attachedQuestionIds);
        });

        // Check if there are enough remaining questions to add
        $countToAdd = $request->input('random_question_count');
        if ($countToAdd > $remainingQuestions->count()) {
            $countToAdd = $remainingQuestions->count();
        }

        // Take random questions from the remaining questions
        $randomQuestions = $remainingQuestions->shuffle()->take($countToAdd);

        // Attach the random questions to the test
        $test->questions()->attach($randomQuestions);

        return redirect()->route('dashboard.tests.show', $id);
    }

    public function all_questions()
    {
        $questions = Question::latest()->get();
        return $questions;
    }

    public function create()
    {

        return view('admin.questions.create');
    }

    public function store(Request $request)
    {


        // Validate the form data
        $request->validate([
            'title' => 'nullable',
            'description' => 'required',
            'value' => 'required|integer',
            'image' => 'nullable',
            'voice' => 'nullable',
            'video' => 'nullable',
        ]);

        // Create a new question

        $question = Question::create([
            'title' => $request->title,
            'description' => $request->description,
            'value' => $request->value,
        ]);

        $uploadedFileName = $this->uploadController->upload_image($request);
        $question->update(['image' => $uploadedFileName]);
        $uploadedFileName = $this->uploadController->upload_voice($request);
        $question->update(['voice' => $uploadedFileName]);
        $uploadedFileName = $this->uploadController->upload_video($request);
        $question->update(['video' => $uploadedFileName]);
        return response()->json(['success' => "true", 'question' => $question], 201);

    }

    public function show($id)
    {
        $question = Question::find($id);
        return view('admin.questions.show', ['question' => $question]);
    }

    public function edit($id)
    {
        $question = Question::findOrFail($id);
        return view('admin.questions.edit', ['question' => $question]);
    }

    public function update(Request $request, Question $question)
    {
        // Validate the form data
//        $validator = Validator::make($request->all(), [
//            'description' => 'required',
//            'value' => 'required|integer',
//            'right_answer' => 'required',
//            'first_wrong_answer' => 'required',
//            'second_wrong_answer' => 'required',
//            'third_wrong_answer' => 'required',
//            'file_description' => 'nullable',
//        ]);

        // Update the question
        if ($request->description) {
            $question->description = $request->description;
            $question->save();
        }
        if ($request->value) {
            $question->value = $request->value;
            $question->save();
        }
        if ($request->title) {
            $question->title = $request->title;
            $question->save();
        }

        $uploadedFileName = $this->uploadController->upload_image($request);
        $question->update(['image' => $uploadedFileName]);
        $uploadedFileName = $this->uploadController->upload_voice($request);
        $question->update(['voice' => $uploadedFileName]);
        $uploadedFileName = $this->uploadController->upload_video($request);
        $question->update(['video' => $uploadedFileName]);
        $questions = Question::all();
        return view('admin.questions.index', ['questions' => $questions]);

    }

    public function destroy($id)
    {


        $list_ids = explode(",", $id);
        DB::table('questions_answers')->whereIn('question_id', $list_ids)->delete();
       Question::whereIn('id', $list_ids)->delete();

        session()->flash('success', 'تم حذف نتيجة الاختبار بنجاح.');

        return response()->json(['success' => true], 201);
    }


}
