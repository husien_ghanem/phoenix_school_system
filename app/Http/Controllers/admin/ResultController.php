<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Models\Answers;
use App\Models\Classes;
use App\Models\Question;
use App\Models\Result;
use App\Models\Sections;
use App\Models\Student;
use App\Models\Test;
use Illuminate\Http\Request;
use Illuminate\Routing\Route;

class ResultController extends Controller
{


    public function index()
    {
        $results = Result::where('student_id', auth('student')->user()->id)->get();

        return view('front.all-results', compact('results'));
    }
    public function previousTests($resultId)
    {
        $result = Result::findOrFail($resultId);
        $answeredQuestionIds = json_decode($result->answered_questions);
        $answeredAnswerIds = json_decode($result->answered_answers);

        $questions = Question::whereIn('id', $answeredQuestionIds)->get();
        $answers = Answers::whereIn('id', $answeredAnswerIds)->get();

        return view('admin.results.previous-tests', compact('result', 'questions', 'answers'));
    }
    public function submittedExamResults(Request $request)
    {
        // Get all unique test_id values from the results table
        $testIds = Result::distinct()->pluck('test_id')->toArray();
        $tests = Test::whereIn('id', $testIds)->get();
        $classIds = $tests->pluck('class_id')->unique()->toArray();
        $classRecord = Classes::whereIn('id', $classIds)->get();


        return view('admin.results.submitted-exam-results', compact('classRecord',));
    }


    public function getViewClassResults($class_id)
    {
        $results = Result::whereHas('test', function ($query) use ($class_id) {
            $query->where('class_id', $class_id);
        })->get();

        // Initialize arrays to store data
        $data = [];

        foreach ($results as $result) {

            $data[] = [
                'student_id'=>$result->student_id ,
                'name' => $result->student->name,
                'section_title' => $result->student->section->title,
                'class_title' => $result->student->section->class->title,
                'test_title' => $result->test->title,
                'result_percent' => $result->result_percent,
                'result_id' => $result->id,
            ];
        }
          return $data ;
    }
    public function ViewClassResults(Request $request)
    {
        $classId = $request->class_id;

        return view('admin.results.section-results');

    }

    public function allResults($studentId)
    {

        $results = Student::with('results')->findOrFail($studentId);

//        return $results ;
        return view('front.all-results-students', compact('results'));
    }

    public function viewTestResults($resultId)
    {
        $result = Result::findOrFail($resultId);
        $answeredQuestionIds = json_decode($result->answered_questions);
        $answeredAnswerIds = json_decode($result->answered_answers);

        $questions = Question::whereIn('id', $answeredQuestionIds)->get();
        $answers = Answers::whereIn('id', $answeredAnswerIds)->get();

        return view('front.view-test-results', compact('result', 'questions', 'answers'));
    }

    public function viewTestResultsForAdmin($resultId)
    {
        $result = Result::findOrFail($resultId);
        $answeredQuestionIds = json_decode($result->answered_questions);
        $answeredAnswerIds = json_decode($result->answered_answers);

        $questions = Question::whereIn('id', $answeredQuestionIds)->get();
        $answers = Answers::whereIn('id', $answeredAnswerIds)->get();

        return view('front.test-result-for-admin', compact('result', 'questions', 'answers'));
    }

    public function deleteTestResults($id)
    {
        $list_ids = explode(",", $id);
        Result::whereIn('id', $list_ids)->delete();
        session()->flash('success', 'تم حذف نتيجة الاختبار بنجاح.');

        return redirect()->back();
    }

}
