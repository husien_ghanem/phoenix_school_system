<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;

use App\Http\Requests\SectionsRequest;
use App\Models\Classes;
use App\Models\Sections;
use Illuminate\Http\Request;
use App\Http\Requests\ClassesRequest;

class SectionsController extends Controller
{
    public function all_sections($id)
    {
        $data = Sections::where('class_id',$id)->get();
        return $data;
    }
// here is
    public function store(Request $request)
    {

        $data = Sections::create($request->all());
        return response()->json(['success' => "true"], 200);
    }
    public function edit($id)
    {
        $data = Sections::find($id);
        return view('admin.sections.edit', compact('data'));
    }
}
