<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Models\Classes;
use App\Models\Sections;
use App\Models\Student;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\Rule;

class StudentController extends Controller
{

    public function index()
    {
        $students = Student::all();
        return view('admin.students.index', ['students' => $students]);
    }

    public function all_students()
    {

        $students = Student::with('section.class')->get();
        return response()->json($students);
    }


    public function create()
    {
        $sections = Sections::with('class')->get();

        return view('admin.students.create', compact('sections'));


    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => ['required', 'max:255'],
            'student_number' => ['required', 'unique:students'],
            'password' => ['required'],
            'section_id' => ['required'],
        ]);

        $students = Student::create([
            'name' => $request->name,
            'student_number' => $request->student_number,
            'section_id' =>  $request->section_id,
            'password' => Hash::make($request->password),
        ]);

        return response()->json(['success' => "true", 'student' => $students], 201);
    }

    public function edit($id)
    {
        $student = Student::with('section.class')->find($id);
        $sections = Sections::all(); // Assuming you have a Section model.
        $classes = Classes::all(); // Assuming you have a Class model.

    return view('admin.students.edit', compact('student', 'sections', 'classes'));
}



    public function update(Request $request, Student $student)
    {
        $request->validate([
            'name' => 'required|max:255',
            'student_number' => [
                'required',
                Rule::unique('students')->ignore($student->id),
            ],
            'password' => 'required',
            'section_id' => ['required'],
        ]);

        // Update the fields if they are present in the request
        if ($request->filled('name')) {
            $student->name = $request->name;
        }

        if ($request->filled('student_number')) {
            $student->student_number = $request->student_number;
        }

        if ($request->filled('password')) {
            $student->password = Hash::make($request->password);

        }
            if ($request->filled('section_id')) {
                $student->section_id = $request->section_id;
            }


        $student->save();

        return $student;

    }

    public function show($id)
    {
        //
    }

    public function destroy($id)
    {
        $list_ids = explode(",", $id);

        foreach ($list_ids as $studentId) {
            $student = Student::find($studentId);
            if ($student) {
                // Delete related results
                $student->results()->delete();
                // Delete the student
                $student->delete();
            }
        }

        return $list_ids;
    }





}
