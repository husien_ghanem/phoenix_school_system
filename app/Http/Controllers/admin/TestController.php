<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\TestRequest;
use App\Models\Classes;
use App\Models\Question;
use App\Models\Test;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

//use Illuminate\Support\Facades\Request;

class TestController extends Controller
{
    public function index()
    {
        $classes = Classes::all();
        $tests=Test::all();
        return view('admin.tests.index',compact('tests','classes'));
    }

    public function all_tests()
    {
        $data = Test::latest()->get();
        return $data;
    }

    public function store(TestRequest $request)
    {

        Test::create($request->all());

        return redirect()->route('dashboard.tests.index');
    }

    public function edit($id)
    {
        $data = Test::find($id);
        $classes = Classes::all();
        return view('admin.tests.edit', compact('data','classes'));
    }

    public function show($id)
    {

        $test = Test::find($id);
        $question_tests = Question::whereHas('tests', function ($query) use ($test) {
            $query->where('test_id', $test->id);
        })->get();
        $questions = Question::all();
        $account_of_questions = Question::count();

        return view('admin.tests.show', compact('test', 'questions','question_tests','account_of_questions'));
    }

    public function add_question(Request $request, $id)
    {

         $request->validate([
             'selected_questions' => 'required|array',
             'selected_questions.*' => 'exists:questions,id',
         ]);
        $test = Test::findOrFail($id);
        $selectedQuestionIds = $request->input('selected_questions');
        $test->questions()->attach($selectedQuestionIds);

        $test->save();

        return redirect()->route('dashboard.tests.show', $id);
    }



    public function update(TestRequest $request, $id)
    {
        $data = Test::find($id);
        $data->update($request->all());
        return response()->json(['success' => "true"], 200);
    }

    public function remove_question($test_id,$question_id ,Request $request)
    {

        DB::table('question_test')
            ->where('test_id', $test_id)
            ->where('question_id', $question_id)
            ->delete();
        return redirect()->route('dashboard.tests.show', $test_id);
    }
    public function destroy($id)
    {

        $test = Test::findOrFail($id);
        $test->questions()->detach();
         $test->delete();
        return redirect()->route('dashboard.tests.index');
    }

}
