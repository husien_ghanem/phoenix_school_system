<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use App\Models\TemporaryFile;
use Illuminate\Support\Facades\Storage;

class UserController extends Controller
{
    // Must delete this after user it
    public function register_first_admin(Request $request)
    {
        $user = User::create([
            'name' => 'AraCoders',
            'email' => 'admin@aracoders.com',
            'password' => Hash::make('aracoders'),
            'role' => 1,
        ]);

        return response()->json(['success' => "true", 'user' => $user]);
    }

    public function index()
    {
        $users = User::all();
        return view('admin.users.index', ['users' => $users]);
    }

    public function all_users()
    {
        $users = User::latest()->get();
        return $users;
    }


    public function create()
    {
        return view('admin.users.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => ['required', 'max:255'],
            'email' => ['required', 'email', 'unique:users'],
            'password' => ['required'],
        ]);

        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'role' => $request->role,
        ]);


        return response()->json(['success' => "true", 'user' => $user], 201);
    }

    public function edit($id)
    {
        $user = User::find($id);
        return view('admin.users.edit', ['user' => $user]);
    }


    public function update(Request $request, User $user)
    {
        $request->validate([
            'email' => ['email', 'unique:users,email,' . $user->id],
        ]);

        if($request->name)
        {
            $user->name = $request->name;
            $user->save();
        }

        if ($request->email) {
            $user->email = $request->email;
            $user->save();
        }

        if ($request->password) {
            $user->password = Hash::make($request->password);
            $user->save();
        }

        return $user;
    }

    public function show($id)
    {
        //
    }

    public function destroy($id)
    {
        $list_ids = explode(",", $id);
        User::whereIn('id', $list_ids)->delete();
        return $list_ids;
    }




}
