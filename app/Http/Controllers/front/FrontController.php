<?php

namespace App\Http\Controllers\front;

use App\Http\Controllers\Controller;
use App\Models\Answers;
use App\Models\Question;
use App\Models\Result;
use App\Models\Test;
use Illuminate\Http\Request;
use Illuminate\Routing\Route;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class FrontController extends Controller
{

    public function showTest(Request $request)
    {

        $selectedTestId = $request->input('selected_test');
        $test = Test::findOrFail($selectedTestId);
        Session::regenerate() ;
        return view('front.selected-test', compact('test', ));
    }

    public function availableTests()
    {
        $student = Auth::guard('student')->user();
        $studentSectionId = $student->section_id;

        // Get the IDs of tests that the student has already submitted
        $submittedTestIds = $student->results()
            ->where('is_submitted', true)
            ->pluck('test_id')// get the id of test where is submitted and put them in array
            ->toArray();

        // Get the tests that are available to the student
        $tests = Test::whereHas('class.sections', function ($query) use ($studentSectionId) {
            $query->where('id', $studentSectionId);
        })
            ->whereNotIn('id', $submittedTestIds) // Exclude tests that have been submitted
            ->get();
        Session::forget('test_completed');
        return view('front.available-tests', compact('tests'));
    }

    public function showResults()
    {
        // Check if the 'test_completed' session variable exists
        if (session('test_completed')) {
            // Retrieve the variables from the session
            $totalQuestionsValue = session('totalQuestionsValue', 0);
            $totalCorrectAnswersValue = session('totalCorrectAnswersValue', 0);
            $percentageCorrect = session('percentageCorrect', 0);
            Session::regenerate() ;


            // Pass the variables to the view
            return view('front.result-test', [
                'totalQuestionsValue' => $totalQuestionsValue,
                'totalCorrectAnswersValue' => $totalCorrectAnswersValue,
                'percentageCorrect' => $percentageCorrect,
            ]);



        } else {
            // Test not completed, redirect to the available tests page or any other suitable page
            return redirect()->route('student.available.tests');
        }
    }




    public function submitTest(Request $request, $test_id)
    {
        session(['test_completed' => true]);

        // Get the submitted answers from the request
        $submittedAnswers = $request->except('_token');

        $questionIds = DB::table('question_test')
            ->where('test_id', $test_id)
            ->pluck('question_id')
            ->toArray();

        // Get all questions related to the specified test
        $questions = Question::whereIn('id', $questionIds)->get();

        // Calculate the total value of questions related to this test
        $totalQuestionsValue = $questions->sum('value'); // Sum of all question values
        $totalCorrectAnswersValue = 0;

        // Initialize arrays to store the question_ids and answer_ids related to submitted answers
        $answeredQuestions = [];
        $answeredAnswers = [];

        foreach ($submittedAnswers as $questionId => $answerId) {
            // Find the corresponding question
            $question = $questions->firstWhere('id', $questionId);

            if ($question) {
                // Check if the submitted answer is correct
                $isCorrect = Answers::where('question_id', $questionId)
                    ->where('id', $answerId)
                    ->where('is_correct', true)
                    ->exists();

                // If the answer is correct, update the total correct answers value
                if ($isCorrect) {
                    $totalCorrectAnswersValue += $question->value;
                }

                // Add the question and answer IDs to their respective arrays
                $answeredQuestions[] = $questionId;
                $answeredAnswers[] = $answerId;
            }
        }

        $percentageCorrect = $totalQuestionsValue > 0
            ? ($totalCorrectAnswersValue / $totalQuestionsValue) * 100
            : 0; // Set to 0 if totalQuestionsValue is 0

        session()->put([
            'totalQuestionsValue' => $totalQuestionsValue,
            'totalCorrectAnswersValue' => $totalCorrectAnswersValue,
            'percentageCorrect' => $percentageCorrect,
        ]);

        // Create a new Result model instance
        $result = new Result();
        $result->student_id = auth('student')->user()->id; // Assuming you want to store the student's ID
        $result->test_id = $test_id;
        $result->result_percent = $percentageCorrect;
        $result->answered_questions = json_encode($answeredQuestions); // Store as JSON array
        $result->answered_answers = json_encode($answeredAnswers); // Store as JSON array
        $result->is_submitted = true;
        $result->save();

        // Redirect to the results page
        return redirect()->route('student.test.result');
    }


}

