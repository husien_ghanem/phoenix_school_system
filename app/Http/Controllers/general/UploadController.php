<?php

namespace App\Http\Controllers\general;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Image;
use File;


class UploadController extends Controller
{
    public function upload_image(Request $request){

        if($request->hasFile('image') && $request->image != null){
            $file = $request->image;
            $name = $file->hashName();
            $filename = time().'.'.$name;
            $file->storeAs('public/media/' , $filename);

            return $filename;

        }

        return '';
    }
    public function upload_voice(Request $request){

        if($request->hasFile('voice') && $request->voice != null){
            $file = $request->voice;
            $name = $file->hashName();
            $filename = time().'.'.$name;
            $file->storeAs('public/media/' , $filename);
            return $filename;
        }
        return '';
    }
    public function upload_video(Request $request){

        if($request->hasFile('video') && $request->video != null){
            $file = $request->video;
            $name = $file->hashName();
            $filename = time().'.'.$name;
            $file->storeAs('public/media/' , $filename);
            return $filename;
        }
        return '';
    }
    public function upload_word(Request $request){

        if($request->hasFile('wordFile') && $request->wordFile != null){
            $file = $request->wordFile;
            $name = $file->hashName();
            $filename = time().'.'.$name;
            $file->storeAs('public/media/word_files/' , $filename);
            return $filename;
        }
        return '';
    }
    public function upload_multi_images(Request $request)
    {
        if ($request->hasFile('images')) {


            foreach ($request->file('images') as $file) {

                $name = $file->hashName();
                $filename = time().'.'.$name;
                $file->storeAs('public/media/' , $filename);

            }
            return $filename;
        }

        return '';
    }


}
