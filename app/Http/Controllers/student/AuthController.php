<?php

namespace App\Http\Controllers\student;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{


    public function authenticate_student(Request $request)

    {

        $validator = Validator::make($request->all(), [
            'student_number' => ['required'],
            'password' => ['required'],
        ]);

        if ($validator->fails()) {
            return response()->json(['success' => 'false', 'error' => $validator->errors()], 422);
        }

        $credentials = $request->only('student_number', 'password');

        if (Auth::guard('student')->attempt($credentials)) {
            $request->session()->regenerate();

            return response()->json(['success' => 'true', 'message' => 'تم تسجيل الدخول بنجاح'], 200);
        }

        // Authentication failed
        return response()->json(['success' => 'false', 'error' => 'معلومات الدخول غير صحيحة'], 422);
    }


    public function logout(Request $request)
    {
        Auth::logout();

        $request->session()->invalidate();

        $request->session()->regenerateToken();

        return redirect()->route('student.login');
    }


}
