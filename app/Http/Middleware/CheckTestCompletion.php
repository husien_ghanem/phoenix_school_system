<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class CheckTestCompletion
{

    public function handle(Request $request, Closure $next)
    {

        if (!session('test_completed')) {

            return $next($request);

        }
        return redirect()->route('student.test.result'); //

    }

}
