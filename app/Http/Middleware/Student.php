<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


class Student
{
    public function handle(Request $request, Closure $next)
    {

        if (Auth::guard('student')->check()){
            return $next($request);
        }

        // Authentication failed, redirect to the student login page
        return redirect()->route('student.login');
    }
}


//            if (Auth::guard('student')->attempt(['student_number' => 123456, 'password' => 123456])){
