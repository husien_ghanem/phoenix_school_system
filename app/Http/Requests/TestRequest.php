<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TestRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'title' => 'required|string',
            'class_id' => 'required|exists:classes,id', // Requires class_id to exist in the 'classes' table
            'shown' => 'required|boolean',
        ];
    }

    public function messages()
    {
        return [
            'title.required' => 'اسم الاختبار مطلوب',
            'title.string' => 'يجب أن يكون اسم الاختبار نصًا',
            'class_id.required' => 'اسم الصف مطلوب',
            'class_id.exists' => 'الصف غير موجود في الجدول المطلوب',
            'shown.required' => 'حقل الاظهار مطلوب',
            'shown.boolean' => 'يجب أن يكون حقل الاظهار قيمة منطقية (صحيح أو خاطئ)',
        ];
    }

}
