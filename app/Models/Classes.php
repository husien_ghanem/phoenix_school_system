<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Classes extends Model
{
    use HasFactory;

    protected $fillable = [
        'title',
    ];

    public function sections()
    {
        return $this->hasMany(Sections::class, 'class_id', 'id');
    }
    public function tests()
    {
        return $this->hasMany(Test::class);
    }
}
