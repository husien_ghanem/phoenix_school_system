<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Student extends Authenticatable
{
    use HasFactory;
    protected $fillable = [
        'name',
        'student_number',
        'password',
        'section_id'
    ];

    public function section()
    {
        return $this->belongsTo(Sections::class, 'section_id');
    }


    public function results()
    {
        return $this->hasMany(Result::class, 'student_id');
    }

}
