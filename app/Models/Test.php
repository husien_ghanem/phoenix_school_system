<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Test extends Model
{
    use HasFactory;
    protected $guarded = [] ;
    public function questions()
    {
        return $this->belongsToMany(Question::class);
    }

    public function class()
    {
        return $this->belongsTo(Classes::class);
    }

    public function results()
    {
        return $this->hasMany(Result::class, 'test_id');
    }
    public function hasResults()
    {
        return $this->results->isNotEmpty();
    }
}
