<?php

namespace App;

use PhpOffice\PhpWord\IOFactory;

class WordParser
{
    public function parseDocument($filePath)
    {
        $phpWord = IOFactory::load($filePath);
        $sections = $phpWord->getSections();
        $parsedData = [];
        $currentQuestion = null;

        foreach ($sections as $section) {
            $elements = $section->getElements();

            foreach ($elements as $element) {
                if ($element instanceof \PhpOffice\PhpWord\Element\TextRun) {
                    $innerElements = $element->getElements();
                    $text = '';

                    foreach ($innerElements as $innerElement) {
                        if ($innerElement instanceof \PhpOffice\PhpWord\Element\Text) {
                            $text .= $innerElement->getText();
                        }
                    }

                    if ($text) {
                        if (preg_match('/Question :/', $text, $matches)) {
                            // Start of a new question
                            if ($currentQuestion) {
                                $parsedData[] = $currentQuestion;
                            }
                            $currentQuestion = [
                                'title' => '',
                                'description' => '',
                                'value' => 0,
                                'answer_choices' => [],
                                'correct_answer' => '',
                            ];
                        } elseif ($currentQuestion) {
                            // Process question details
                            if (preg_match('/Title:\s*\[(.+?)\]/', $text, $matches)) {
                                $currentQuestion['title'] = $matches[1];
                            } elseif (preg_match('/Description:\s*\[(.+?)\]/', $text, $matches)) {
                                $currentQuestion['description'] = $matches[1];
                            } elseif (preg_match('/Value:\s*(\d+)/', $text, $matches)) {
                                $currentQuestion['value'] = (int) $matches[1];
                            } elseif (preg_match('/Answer Choices (\d+):\s*\[([^\]]+)\]/', $text, $matches)) {
                                $answerChoicesSection = (int) $matches[1];
                                $answerChoice = $matches[2];
                                $currentQuestion['answer_choices'][$answerChoicesSection] = $answerChoice;
                            } elseif (preg_match('/Correct Answer:\s*\[([^\]]+)\]/', $text, $matches)) {
                                $currentQuestion['correct_answer'] = strtoupper($matches[1]);
                            }
                        }
                    }
                }
            }
        }

        // Add the last question to the parsed data
        if ($currentQuestion) {
            $parsedData[] = $currentQuestion;
        }

        return $parsedData;
    }
}
