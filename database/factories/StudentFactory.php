<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\Student;
use Illuminate\Support\Str;


class StudentFactory extends Factory
{

    public function definition(): array
    {
        return [
            'name' => fake()->name(),
            'student_number' => $this->faker->unique()->randomNumber(6),
            'password' => bcrypt('password'), // You can change this to set a default password
            'section_id' => 1 ,
            'remember_token' => Str::random(10),
            'created_at' => now(),
            'updated_at' => now(),
        ];
    }

    public function unverified(): static
    {
        return $this->state(fn(array $attributes) => [
            'email_verified_at' => null,
        ]);
    }
}
