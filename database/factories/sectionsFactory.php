<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\sections>
 */
class sectionsFactory extends Factory
{
    public function definition(): array
    {
        return [
            'title' => 'first section',
            'class_id' => $this->faker->unique()->randomNumber(6),
            'created_at' => now(),
            'updated_at' => now(),
        ];
    }


}
