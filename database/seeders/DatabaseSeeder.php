<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        \App\Models\User::factory()->create([
            'name' => 'Admin',
            'email' => 'admin@admin.com',
            'password' => Hash::make('admin'),
            'role' => 1,
        ]);

        {
            \App\Models\Classes::factory()->create([
                'title' => 'first class',
            ]);
        }
        \App\Models\sections::factory()->create([
            'title' => 'first section title',
            'class_id' => 1,
        ]);

        \App\Models\Student::factory()->create([
            'name' => 'husien',
            'student_number' => 123456,
       'password' => Hash::make('123456'),
        ]);
    }
}
