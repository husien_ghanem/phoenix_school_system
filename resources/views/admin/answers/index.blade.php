\@extends('admin.master')
@section('page-title', 'إضافة إجابات')
@section('navbar-title', 'إضافة إجابات')

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card my-4">
                <div class="card-header p-0 position-relative mt-n4 mx-3 z-index-2">
                    <div class="bg-gradient-primary shadow-primary border-radius-lg pt-4 pb-3">
                        <h6 class="text-white text-capitalize pe-3">إضافة إجابات للسؤال</h6>
                    </div>
                </div>
                <div class="card-body px-0 pb-2">
                    <h5 class="mb-4">السؤال:</h5>
                    <div class="mb-4">
                        <p><strong>العنوان:</strong> {{ $data->title }}</p>
                        <p><strong>الوصف:</strong> {{ $data->description }}</p>
                        <p><strong>القيمة:</strong> {{ $data->value }}</p>
                    </div>

                    <h5 class="mb-4">الإجابات:</h5>
                    <div class="table-responsive p-0">
                        <table class="table align-items-center mb-0 table-hover">
                            <thead>
                            <tr>
                                <th class="text-uppercase text-secondary text-s font-weight-bolder opacity-7 ">نص الإجابة</th>
                                <th class="text-uppercase text-secondary text-s font-weight-bolder opacity-7">إجابة صحيحة</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($answers as $answer)
                                <tr>
                                    <td class="bg-gradient-primary shadow-primary border-radius-lg pt-4 pb-3">   <span class="badge badge-success ">{{ $answer->answer_text }}</span></td>
                                    <td>
                                        @if($answer->is_correct)
                                            <span class="badge badge-success dark-version">نعم</span>
                                        @else
                                            <span class="badge badge-danger dark-version">لا</span>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>

                    <!-- Add Answer Form -->
                    <h5 class="mt-5 mb-4">إضافة إجابة جديدة:</h5>
                    <form action="{{ route('dashboard.answers.store') }}" method="POST">
                        @csrf
                        <input type="hidden" name="question_id" value="{{ $data->id }}">
                        <div class="mb-3">
                            <label for="answer_text" class="form-label">نص الإجابة:</label>
                            <textarea class="form-control" id="answer_text" name="answer_text" required></textarea>
                        </div>
                        <div class="mb-3 form-check">
                            <input type="checkbox" class="form-check-input" id="is_correct" name="is_correct" value="1">
                            <label class="form-check-label" for="is_correct">إجابة صحيحة</label>
                        </div>
                        <button type="submit" class="btn btn-success">إضافة الإجابة</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

