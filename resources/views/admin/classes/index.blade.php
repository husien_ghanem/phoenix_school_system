@extends('admin.master')
@section('page-title', 'الصفوف')
@section('navbar-title', 'الصفوف')

@section('content')
<div class="row">
  <div class="col-12">
    <div class="card my-4">
      <div class="card-header p-0 position-relative mt-n4 mx-3 z-index-2">
        <div class="bg-gradient-primary shadow-primary border-radius-lg pt-4 pb-3">
          <h6 class="text-white text-capitalize pe-3">جدول الصفوف المتوفرة</h6>
        </div>
      </div>
      <div class="card-body px-0 pb-2">

        <div class="col-lg-12 col-md-12 px-2">
          <div style="display: none" class="alert alert-danger alert-dismissible text-white" role="alert" id="alert_message">
            <span class="text-sm"></span>
            <ul class="text-sm m-0" id="errors_list"></ul>
            <button type="button" class="btn-close text-lg py-3 opacity-10" data-bs-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <script>
            $("#alert_message").on("close.bs.alert", function() {
              // hide the alert >>> not remove it
              $("#alert_message").hide();
              return false;
            });
          </script>


          <form role="form" id="add_classes_form" class="text-end" enctype="multipart/form-data">

            <div class="row">
              <div class="col-12 text-center my-3">
                <h4>إضافة صف جديدة</h4>
              </div>
              <div class="col-12 col-md-12">
                <label class="form-label">اسم الصف</label>
                <div class="input-group input-group-outline my-2">

                  <input id="title" name="title" type="text" class="form-control">
                </div>
              </div>


            </div>




            <div class="row">

              <div class="col-12  my-3">
                <button type="submit" class="btn bg-gradient-success w-100 mt-2 text-lg">
                  <span id="button_submit_text"><i class="fa fa-plus"></i> إضافة</span>
                  <div class="spinner" style="display: none;">
                    <div class="double-bounce1"></div>
                    <div class="double-bounce2"></div>
                  </div>
                </button>
              </div>
            </div>
          </form>


        </div>
        <hr />
        <h4 class="my-3 text-center">جدول الصفوف المضافة</h4>
        <div class="table-responsive p-0">
          <table class="table align-items-center mb-0 table-hover " id="classes_tabble" style="width:100%">
            <thead>
              <tr>
                <th class="text-uppercase text-secondary text-s font-weight-bolder opacity-7 ps-2"> اسم الصف </th>
                <th class="text-secondary text-s opacity-7">الاجراءات</th>
              </tr>
            </thead>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>

<!--     toast start     -->
<div class="position-fixed top-0 start-0 p-lg-3" style="z-index: 11">
  <div class="toast fade hide p-2 mt-2 bg-gradient-info" role="alert" aria-live="assertive" id="infoToast" aria-atomic="true">
    <div class="toast-header bg-transparent border-0">
      <i class="material-icons text-white ms-2">notifications</i>
      <span class="ms-auto text-white font-weight-bold text-lg">اشعار</span>
      <i class="fas fa-times text-md text-white me-3 cursor-pointer" data-bs-dismiss="toast" aria-label="Close"></i>
    </div>
    <hr class="horizontal light m-0">
    <div class="toast-body text-white text-lg">
      يتم الآن الحذف ....
    </div>
  </div>

  <div class="toast fade hide p-2 bg-white" role="alert" aria-live="assertive" id="successToast" aria-atomic="true">
    <div class="toast-header border-0">
      <i class="material-icons text-success ms-2">check</i>
      <span class="ms-auto font-weight-bold text-lg">نجاح</span>
      <i class="fas fa-times text-md me-3 cursor-pointer" data-bs-dismiss="toast" aria-label="Close"></i>
    </div>
    <hr class="horizontal dark m-0">
    <div class="toast-body text-lg">
      تمت العملية بنجاح
    </div>
  </div>

</div>
<!--     toast end     -->



<script>
  $(document).ready(function() {
    $.get("{{ route('dashboard.classes.all') }}").done(function(data) {
      var table = $('#classes_tabble').DataTable({
        pagingType: 'full_numbers',
        retrieve: true,
        pageLength: 10,
        processing: true,
        lengthMenu: [5, 10, 25, 50],
        searching: true,
        responsive: true,
        deferRender: true,
        select: false,

        "language": {
          "lengthMenu": "عرض _MENU_ سطر في الصفحة",
          "zeroRecords": "لم يتم ايجاد اي عنصر - عذراً",
          "info": "عرض الصفحة _PAGE_ من _PAGES_",
          "infoEmpty": "لا يوجد اي عنصر متطابق",
          "infoFiltered": "(تم البحث في _MAX_  سطر)",
          "loadingRecords": "يتم التحميل ...",
          "processing": "تتم المعالجة ...",
          "search": "بحث :",
          "paginate": {
            "first": "البداية",
            "last": "النهاية",
            "next": "التالي",
            "previous": "السابق"
          },
        },

        "data": data,
        "order": [],
        "columns": [

          {
            "data": "title",
            "render": function(data, type, row) {
              return '<a href="classes/' + row['id'] + '" >'+data+'</a>';
            }
          },
          {
            "data": "id",
            "render": function(data, type, row) {
              return `
                  <button class="btn btn-info" id='edit_classes'><i class = "fa fa-wrench"> </i></button> `;
            },
          },

        ],

      });







      $('#classes_tabble tbody').on('click', '#edit_classes', function() {
        var data = table.row($(this).parents('tr')).data();

        // if the table in responsive mode and the tr has a child
        if (!data) {
          var data = table.row($(this).closest('tr :not(.child)')).data();
        }

        window.location.href = "/dashboard/classes/" + data['id'] + "/edit";
      });
    });
  });
</script>

{{-- add new classes method --}}
<script>
  $('#add_classes_form').on('submit', function(e) {
    e.preventDefault();

    $("#button_submit_text").hide();
    $(".spinner").show();
    $("#alert_message").hide();

    var uploadData = new FormData(this);

    $.ajax({
      type: "POST",
      headers: {
        'X-CSRF-TOKEN': '{{ csrf_token() }}'
      },
      url: '{{route('dashboard.classes.store')}}',
      data: uploadData,
      contentType: false,
      processData: false,
      statusCode: {
        422: function(error) {
          // Only if your server returns a 422 status code can it come in this block. :-)
          var errors = error.responseJSON.errors;
          // console.log(errors);
          var alert_message = document.getElementById("alert_message");
          $("#alert_message").show();
          var ul = document.getElementById("errors_list");
          $('#errors_list').empty();
          Object.entries(errors).forEach(function([key, value]) {
            var li = document.createElement("li");
            li.appendChild(document.createTextNode(value));
            ul.appendChild(li);
          });
        },
        403: function() {
          // Only if your server returns a 403 status code can it come in this block. :-)
        }
      },
      success: function(data) {
        console.log("success");
        console.log(data);
        $("#button_submit_text").show();
        $(".spinner").hide();
        window.location = '{{route('dashboard.classes.index')}}';
      },
      error: function(error) {
        console.log("error");
        console.log(error);
        $("#button_submit_text").show();
        $(".spinner").hide();
        // myToast.show();
      }
    });
  });
</script>

@endsection
