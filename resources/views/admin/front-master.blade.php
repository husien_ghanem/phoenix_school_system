<!DOCTYPE html>
<html lang="ar" dir="rtl">

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="icon" type="image/png" href="{{ asset('img/logo/logo.png') }}">
    <title>
        @yield('page-title', 'الاختبارات') | Phoenix
    </title>
    <!--     Fonts and icons     -->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Cairo:wght@300&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Material+Symbols+Outlined:opsz,wght,FILL,GRAD@48,400,1,0" />
    <!-- Nucleo Icons -->
    <link href="{{ asset('admin/css/nucleo-icons.css') }}" rel="stylesheet" />
    <link href="{{ asset('admin/css/nucleo-svg.css') }}" rel="stylesheet" />
    <!-- Font Awesome Icons -->
    <script src="https://kit.fontawesome.com/42d5adcbca.js" crossorigin="anonymous"></script>
    <!-- Material Icons -->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons+Round" rel="stylesheet">
    <!-- CSS Files -->
    <link id="pagestyle" href="{{ asset('admin/css/material-dashboard.css?v=3.0.0') }}" rel="stylesheet" />
    <!-- datatable -->
    <link rel="stylesheet" type="text/css" href="{{ asset('admin/js/datatables/datatables.min.css') }}" />
    {{-- tom-select --}}
    <link href="https://cdn.jsdelivr.net/npm/tom-select@2.1.0/dist/css/tom-select.css" rel="stylesheet">

    {{-- custom css --}}
    <link rel="stylesheet" href="{{ asset('admin/css/new_style.css') }}">
    <!-- Filepond stylesheet -->
    <link href="{{ asset('admin/js/filepond-master/filepond.min.css') }}" rel="stylesheet" />
    <!--   Core JS Files   -->
    <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.5/jquery.validate.min.js" integrity="sha512-rstIgDs0xPgmG6RX1Aba4KV5cWJbAMcvRCVmglpam9SoHZiUCyQVDdH2LPlxoHtrv17XWblE/V/PP+Tr04hbtA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script defer src="{{ asset('admin/js/core/popper.min.js') }}"></script>
    <script defer src="{{ asset('general/js/general.js') }}"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous">
    </script>
    <script defer src="{{ asset('admin/js/plugins/perfect-scrollbar.min.js') }}"></script>
    <script defer src="{{ asset('admin/js/plugins/smooth-scrollbar.min.js') }}"></script>

    <!-- Github buttons -->
    <script async defer src="https://buttons.github.io/buttons.js"></script>
    <!-- Control Center for Material Dashboard: parallax effects, scripts for the example pages etc -->
    <script defer src="{{ asset('admin/js/material-dashboard.js?v=3.0.0') }}"></script>

    <!-- datatable -->
    <script type="text/javascript" src="{{ asset('admin/js/datatables/datatables.min.js') }}"></script>

    {{-- sweetAlert --}}
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>

    {{-- tom select --}}
    <script src="https://cdn.jsdelivr.net/npm/tom-select@2.1.0/dist/js/tom-select.complete.min.js"></script>

    <!-- Load FilePond library -->
    <script src="{{ asset('admin/js/filepond-master/filepond.min.js') }}"></script>
    <script src="{{ asset('admin/js/filepond-master/filepond.min.js') }}"></script>
</head>

<body class="g-sidenav-show rtl bg-gray-200">


<aside class="sidenav navbar navbar-vertical navbar-expand-xs border-0 border-radius-xl my-3 fixed-end me-3 rotate-caret  bg-gradient-dark" id="sidenav-main">
    <div class="sidenav-header">
        <i class="fas fa-times p-3 cursor-pointer text-white opacity-5 position-absolute start-0 top-0 d-none d-xl-none" aria-hidden="true" id="iconSidenav"></i>
        <a class="navbar-brand m-0 text-center mb-2" href="#">
            <img src="{{ asset('img/logo/logo.png') }}" class="navbar-brand-img h-100" alt="main_logo"> <br />
            <span class="me-1 font-weight-bold text-white">Phoenix</span><br />
        </a>
    </div>
    <hr class="horizontal light mt-2 mb-2">
    <div class="collapse navbar-collapse px-0 w-auto  max-height-vh-100">
        <ul class="navbar-nav">

            <li class="nav-item">
                <a class="nav-link text-white {{ request()->routeIs('dashboard.index') ? 'active' : '' }} " href="{{ route('student.all-results') }}">
                    <div class="text-white text-center ms-2 d-flex align-items-center justify-content-center ">
                        <i class="material-icons opacity-10">dashboard</i>
                    </div>
                    <span class="nav-link-text me-1">الاختبارات السابقة</span>
                </a>
            </li>

            <li class="nav-item">
                <a class="nav-link text-white {{ request()->routeIs('dashboard.index') ? 'active' : '' }} " href="{{ route('student.available.tests') }}">
                    <div class="text-white text-center ms-2 d-flex align-items-center justify-content-center ">
                        <i class="material-icons opacity-10">dashboard</i>
                    </div>
                    <span class="nav-link-text me-1">الاختبارات المتاحة</span>
                </a>
            </li>
            <li class="nav-item d-lg-none d-md-none  ">
                <a class="nav-link text-white " href="{{ route('logout') }}">
                    <div class="text-white text-center ms-2 d-flex align-items-center justify-content-center">
                        <i class="material-icons opacity-10">logout</i>
                    </div>
                    <span class="nav-link-text me-1">تسجيل خروج</span>
                </a>
            </li>

        </ul>
    </div>

</aside>


<main class="main-content position-relative max-height-vh-100 h-100 border-radius-lg overflow-x-hidden">
    <!-- Navbar -->
    <nav class="navbar navbar-main navbar-expand-lg px-0 mx-4 shadow-none border-radius-xl" id="navbarBlur" navbar-scroll="true">
        <div class="container-fluid py-1 px-3">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb bg-transparent mb-0 pb-0 pt-1 px-0 ">
                    <li class="breadcrumb-item text-sm ps-2"><a class="opacity-5 text-dark" href="javascript:;">قائمة
                            الاختبارات </a></li>
                    <li class="breadcrumb-item text-sm text-dark active" aria-current="page">
                        @yield('navbar-title')
                    </li>
                </ol>

            </nav>
            <div class="collapse navbar-collapse mt-sm-0 mt-2 px-0" id="navbar">

                <ul class="navbar-nav me-auto ms-0 justify-content-end">
                    <li class="nav-item d-flex align-items-center d-sm-inline d-none">
                        <a href="{{ route('logout') }}" class="nav-link text-body font-weight-bold px-0">
                            <i class="fas fa-sign-out-alt "></i>
                            <span class="d-sm-inline d-none">تسجيل خروج </span>
                        </a>
                    </li>
                    <li class="nav-item d-xl-none pe-3 d-flex align-items-center">
                        <a href="javascript:;" class="nav-link text-body p-0" id="iconNavbarSidenav">
                            <div class="sidenav-toggler-inner">
                                <i class="sidenav-toggler-line"></i>
                                <i class="sidenav-toggler-line"></i>
                                <i class="sidenav-toggler-line"></i>
                            </div>
                        </a>
                    </li>

                </ul>
            </div>
        </div>
    </nav>
    <!-- End Navbar -->


    <div class="container-fluid py-4">
        <div class="row min-vh-80 h-100">
            <div class="col-12">

                @yield('content')


            </div>
        </div>




        <footer class="footer py-4  ">
            <div class="container-fluid">
                <div class="row align-items-center justify-content-lg-between">
                    <div class="col-lg-6 mb-lg-0 mb-4">

                    </div>
                </div>
            </div>
        </footer>
    </div>



</main>

</body>

</html>
