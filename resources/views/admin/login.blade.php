<!DOCTYPE html>
<html lang="ar" dir="rtl">

<head>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="apple-touch-icon" sizes="76x76" href="{{asset('img/admin/apple-icon.png')}}">
  <link rel="icon" type="image/png" href="{{asset('img/admin/favicon.png')}}">
  <title>
    Admin | Login
  </title>
  <!--     Fonts and icons     -->
  <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900|Roboto+Slab:400,700" />
  <!-- Nucleo Icons -->
  <link href="{{asset('admin/css/nucleo-icons.css')}}" rel="stylesheet" />
  <link href="{{asset('admin/css/nucleo-svg.css')}}" rel="stylesheet" />
  <!-- Font Awesome Icons -->
  <script src="https://kit.fontawesome.com/42d5adcbca.js" crossorigin="anonymous"></script>
  <!-- Material Icons -->
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons+Round" rel="stylesheet">
  <!-- CSS Files -->
  <link id="pagestyle" href="{{asset('admin/css/material-dashboard.css?v=3.0.0')}}" rel="stylesheet" />
  <link rel="stylesheet" href="{{asset('admin/css/new_style.css')}}">

  <!--   Core JS Files   -->
  <script  src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
  <script defer src="{{asset('admin/js/core/popper.min.js')}}"></script>
  <script defer src="{{asset('admin/js/core/bootstrap.min.js')}}"></script>
  <script defer src="{{asset('admin/js/plugins/perfect-scrollbar.min.js')}}"></script>
  <script defer src="{{asset('admin/js/plugins/smooth-scrollbar.min.js')}}"></script>
  <!-- Github buttons -->
  <script async defer src="https://buttons.github.io/buttons.js"></script>
  <!-- Control Center for Material Dashboard: parallax effects, scripts for the example pages etc -->
  <script defer src="{{asset('admin/js/material-dashboard.js?v=3.0.0')}}"></script>

</head>

<body class="bg-gray-200">


  <main class="main-content  mt-0">


      <div class="page-header align-items-end min-vh-100" style="background-image: url('https://images.unsplash.com/photo-1497294815431-9365093b7331?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1950&q=80');">


      <span class="mask bg-gradient-dark opacity-6"></span>

          <div class="container my-auto">

              <div class="row">
          <div class="col-lg-4 col-md-8 col-12 mx-auto">
            <div class="card z-index-0 fadeIn3 fadeInBottom">
              <div class="card-header p-0 position-relative mt-n4 mx-3 z-index-2">
                <div class="bg-gradient-primary shadow-primary border-radius-lg py-3 ps-1">
                  <h4 class="text-white font-weight-bolder text-center mt-2 mb-0">تسجيل الدخول كمسؤول</h4>
                  <div class="row mt-3"></div>
                </div>
              </div>
              <div class="card-body">
                  <div class="text-center">
                      <img src="{{ asset('storage/media/phoenix.png') }}" class="img-fluid" style="width: 150px;" />
                  </div>

                <form role="form" id="login_admin_form" class="text-end">
                  <div class="input-group input-group-outline my-1">
                    <label class="form-label">اسم المستخدم</label>
                    <input id="email" type="text" class="form-control">
                  </div>
                  <div class="input-group input-group-outline mb-3">
                    <label class="form-label">كلمة المرور</label>
                    <input id="password" type="password" class="form-control">
                  </div>

                  <div class="text-center">
                    <button type="submit" class="btn bg-gradient-primary w-100 my-4 mb-2 text-lg">
                      <span id="button_submit_text">دخول</span>
                      <div class="spinner" style="display: none;">
                        <div class="double-bounce1"></div>
                        <div class="double-bounce2"></div>
                      </div>
                    </button>
                  </div>

                </form>
              </div>
            </div>
          </div>
        </div>



        <!--     toast start     -->
        <div class="position-fixed top-0 start-0 p-lg-3" style="z-index: 11">
          <div class="toast fade hide p-2 mt-2 bg-white " role="alert" aria-live="assertive" id="dangerToast" aria-atomic="true">
            <div class="toast-header border-0">
              <i class="material-icons text-danger ms-2"> campaign</i>
              <span class="ms-auto text-gradient text-danger font-weight-bold text-lg">خطأ   </span>
              {{-- <small class="text-body">11 mins ago</small> --}}
              <i class="fas fa-times text-md me-3 cursor-pointer" data-bs-dismiss="toast" aria-label="Close"></i>
            </div>
            <hr class="horizontal dark m-0">
            <div class="toast-body text-lg">
              البيانات المدخلة غير صحيحة
            </div>
          </div>
        </div>
        <!--     toast end     -->

      </div>


    </div>




  </main>



<script>
    $('#login_admin_form').on('submit', function(e) {
       e.preventDefault();

       var myToastEl = document.getElementById('dangerToast');
       var myToast = bootstrap.Toast.getInstance(myToastEl);


       $("#button_submit_text").hide();
       $(".spinner").show();

       var uploadData = new FormData();

       var email = $('#email').val();
       var password = $('#password').val();
       var rememberMe = false;
       if ($('#rememberMe').is(':checked')) {
         var rememberMe = true;
         }



       uploadData.append('email',email);
       uploadData.append('password',password);
       uploadData.append('rememberMe',rememberMe);


       $.ajax({
           type: "POST",
           headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}'},
           url: '{{route('admin.authenticate')}}',
           data: uploadData,
           contentType: false,
           processData: false,
           statusCode: {
                422: function() {
                    // Only if your server returns a 422 status code can it come in this block. :-)
                    myToast.show();
                },
                403: function() {
                    // Only if your server returns a 422 status code can it come in this block. :-)
                }
            },
           success:function(data){
                console.log("success");
                console.log(data);
                $("#button_submit_text").show();
                $(".spinner").hide();
                window.location = '{{route('dashboard.index')}}';
           },
           error:function(error){
                console.log("error");
                console.log(error);
                $("#button_submit_text").show();
                $(".spinner").hide();
                // myToast.show();
           }
       });
   });
</script>


</body>

</html>
