@extends('admin.master')
@section('navbar-title', 'إضافة سؤال')

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header pb-0 px-3">
                    <h6 class="mb-0 text-lg">إضافة سؤال جديد</h6>
                </div>

                <div class="card-body pt-4 p-3">
                    <div style="display: none" class="alert alert-danger alert-dismissible text-white" role="alert" id="alert_message">
                        <span class="text-sm"></span>
                        <ul class="text-sm m-0" id="errors_list"></ul>
                        <button type="button" class="btn-close text-lg py-3 opacity-10" data-bs-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <script>
                        $("#alert_message").on("close.bs.alert", function () {
                            // hide the alert >>> not remove it
                            $("#alert_message").hide();
                            return false;
                        });
                    </script>

                    <form role="form" id="add_question_form" class="text-end" enctype="multipart/form-data">
                        @csrf
                        <div class="input-group input-group-outline my-3">
                            <label class="form-label">العنوان</label>
                            <input id="title" type="text" class="form-control">
                        </div>
                        <div class="input-group input-group-outline my-3">
                            <label class="form-label">الوصف</label>
                            <input id="description" type="text" class="form-control">
                        </div>
                        <div class="input-group input-group-outline my-3">
                            <label class="form-label">القيمة</label>
                            <input id="value" type="number" class="form-control">
                        </div>
                        <div class="input-group input-group-outline my-3">
                            <label class="custom-file-input ">رفع صورة (اختياري)</label>
                            <input id="image" type="file" class="custom-file-input ">
                        </div>
                        <div class="input-group input-group-outline my-3">
                            <label class="custom-file-input">رفع صوت (اختياري)</label>
                            <input id="voice" type="file" class="custom-file-input ">
                        </div>
                        <div class="input-group input-group-outline my-3">
                            <label class="custom-file-input">رفع فيديو (اختياري)</label>
                            <input id="video" type="file" class="custom-file-input ">
                        </div>

                        <div class="text-center">
                            <button type="submit" class="btn bg-gradient-primary w-50 my-4 mb-2 text-lg">
                                <span id="button_submit_text">إضافة</span>
                                <div class="spinner" style="display: none;">
                                    <div class="double-bounce1"></div>
                                    <div class="double-bounce2"></div>
                                </div>
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <script>
        $('#add_question_form').on('submit', function (e) {
            e.preventDefault();

            $("#button_submit_text").hide();
            $(".spinner").show();
            $("#alert_message").hide();

            var uploadData = new FormData();

            var description = $('#description').val();
            var value = $('#value').val();
            var title = $('#title').val();
            var image = $('#image')[0].files[0];
            var voice = $('#voice')[0].files[0];
            var video = $('#video')[0].files[0];

            uploadData.append('description', description);
            uploadData.append('value', value);
            uploadData.append('title', title);
            uploadData.append('image', image);
            uploadData.append('voice', voice);
            uploadData.append('video', video);

            $.ajax({
                type: "POST",
                headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}'},
                url: '{{ route('dashboard.questions.store') }}',
                data: uploadData,
                contentType: false,
                processData: false,
                statusCode: {
                    422: function (error) {
                        var errors = error.responseJSON.errors;
                        var alert_message = document.getElementById("alert_message");
                        $("#alert_message").show();
                        var ul = document.getElementById("errors_list");
                        $('#errors_list').empty();
                        Object.entries(errors).forEach(function ([key, value]) {
                            var li = document.createElement("li");
                            li.appendChild(document.createTextNode(value));
                            ul.appendChild(li);
                        });
                    },
                    403: function () {
                        // Only if your server returns a 403 status code can it come in this block. :-)
                    }
                },
                success: function (data) {

                    console.log(data);
                    $("#button_submit_text").show();
                    $(".spinner").hide();

                    // Redirect to the index page after a successful store
                    window.location = '{{ route('dashboard.questions.answers', ['question' => ':question_id']) }}'.replace(':question_id', data.question.id);

                },
                error: function (error) {
                    $("#button_submit_text").show();
                    $(".spinner").hide();
                    // myToast.show();
                }
            });
        });
    </script>
@endsection
