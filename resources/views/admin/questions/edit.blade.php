@extends('admin.master')
@section('page-title', 'تعديل السؤال')
@section('navbar-title', 'تعديل السؤال')

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card my-4">
                <div class="card-header p-0 position-relative mt-n4 mx-3 z-index-2">
                    <div class="bg-gradient-primary shadow-primary border-radius-lg pt-4 pb-3">
                        <h6 class="text-white text-capitalize pe-3">تعديل السؤال</h6>
                    </div>
                </div>
                <div class="card-body">
                    <!-- Edit Form -->
                    <form method="POST" action="{{ route('dashboard.questions.update', $question->id) }}"
                          enctype="multipart/form-data">
                        @csrf
                        @method('PUT')

                        <div class="mb-3">
                            <label for="title" class="form-label">عنوان السؤال</label>
                            <input type="text" class="form-control" id="title" name="title" value="{{ $question->title }}">
                        </div>

                        <div class="mb-3">
                            <label for="description" class="form-label">وصف السؤال</label>
                            <textarea class="form-control" id="description" name="description"
                                      rows="4">{{ $question->description }}</textarea>
                        </div>

                        <div class="mb-3">
                            <label for="value" class="form-label">القيمة</label>
                            <input type="number" class="form-control" id="value" name="value" value="{{ $question->value }}">
                        </div>

                        <div class="mb-3">
                            <label for="image" class="form-label">صورة السؤال</label>
                            <input type="file" class="form-control" id="image" name="image">
                            @if ($question->image)
                                <img src="/storage/media/{{ $question->image }}" alt="Question Image" class="mt-2"
                                     style="max-width: 100px;">
                            @endif
                        </div>

                        <div class="mb-3">
                            <label for="voice" class="form-label">ملف صوتي</label>
                            <input type="file" class="form-control" id="voice" name="voice">
                            @if ($question->voice)
                                <audio controls class="mt-2">
                                    <source src="/storage/media/{{ $question->voice }}" type="audio/mpeg">
                                    Your browser does not support the audio element.
                                </audio>
                            @endif
                        </div>

                        <div class="mb-3">
                            <label for="video" class="form-label">ملف فيديو</label>
                            <input type="file" class="form-control" id="video" name="video">
                            @if ($question->video)
                                <video controls class="mt-2" style="max-width: 300px;">
                                    <source src="/storage/media/{{ $question->video }}" type="video/mp4">
                                    Your browser does not support the video tag.
                                </video>
                            @endif
                        </div>

                        <button type="submit" class="btn btn-primary">حفظ التغييرات</button>
                    </form>
                    <!-- End Edit Form -->
                </div>
            </div>
        </div>
    </div>


<script>
        $('#edit_question_form').on('submit', function (e) {
            e.preventDefault();

            $("#button_submit_text").hide();
            $(".spinner").show();
            $("#alert_message").hide();
            $("#success_message").hide();

            var uploadData = new FormData();

            var description = $('#description').val();
            var value = $('#value').val();
            var title = $('#title').val();
            var image = $('#image')[0].files[0];
            var voice = $('#voice')[0].files[0];
            var video = $('#video')[0].files[0];

            uploadData.append('description', description);
            uploadData.append('value', value);
            uploadData.append('title', title);
            uploadData.append('voice', voice);
            uploadData.append('video', video);
            uploadData.append('image', image);
            uploadData.append('_method', 'PUT');

            $.ajax({
                type: "POST",
                headers: { 'X-CSRF-TOKEN': '{{ csrf_token() }}' },
                url: '{{ route('dashboard.questions.update', $question) }}',
                data: uploadData,
                contentType: false,
                processData: false,
                statusCode: {
                    422: function (error) {
                        // Only if your server returns a 422 status code can it come in this block. :-)
                        var errors = error.responseJSON.errors;
                        // console.log(errors);
                        var alert_message = document.getElementById("alert_message");
                        $("#alert_message").show();
                        var ul = document.getElementById("errors_list");
                        $('#errors_list').empty();
                        Object.entries(errors).forEach(function ([key, value]) {
                            var li = document.createElement("li");
                            li.appendChild(document.createTextNode(value));
                            ul.appendChild(li);
                        });
                    },
                    403: function () {
                        // Only if your server returns a 403 status code can it come in this block. :-)
                    }
                },
                success: function (data) {
                    console.log("success");
                    console.log(data);
                    $("#button_submit_text").show();
                    $(".spinner").hide();
                    $("#success_message").show();
                    window.location = window.location;
                },
                error: function (error) {
                    console.log("error");
                    console.log(error);
                    $("#button_submit_text").show();
                    $(".spinner").hide();
                    $("#alert_message").show();
                    // myToast.show();
                }
            });
        });
    </script>

@endsection
