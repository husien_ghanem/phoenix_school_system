@extends('admin.master')
@section('page-title', 'الأسئلة')
@section('navbar-title', 'الأسئلة')

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card my-4">
                <div class="card-header p-0 position-relative mt-n4 mx-3 z-index-2">
                    <div class="bg-gradient-primary shadow-primary border-radius-lg pt-4 pb-3">
                        <h6 class="text-white text-capitalize pe-3">جدول الأسئلة</h6>
                    </div>
                </div>
                <div class="card-body px-0 pb-2">
                    <form action="{{ route('dashboard.questions.upload') }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="mb-3">

                            <label for="wordFile"  class="badge badge-secondary text-danger card m-3">تحميل الأسئلة من ملف Word</label>
                            <input type="file" class="custom-file-input"  id="wordFile" name="wordFile" accept=".docx">

                            <button type="submit" class="btn btn-primary mt-2" data-toggle="tooltip" data-placement="left" title="اضغط لتحميل الاسئلة من ملف وورد">
                                رفع الملف
                            </button>
                        </div>
                    </form>

                    <div class="button_datatable px-2">
                        <a href="{{ route('dashboard.questions.create') }}" class="btn btn-success text-sm" id="add_new_question_button" type="button">
                            <i class="fa fa-plus"></i>
                            إضافة سؤال جديد
                        </a>
                        <button class="btn btn-primary text-sm" id="selected_all_rows_button" type="button" >
                            <i class="fa fa-check-square-o"></i>
                            تحديد الكل
                        </button>
                        <button class="btn btn-secondary text-sm" id="deselected_all_rows_button" type="button" >
                            <i class="fa fa-times-circle"></i>
                            الغاء التحديد
                        </button>
                        <button class="btn btn-danger text-sm" id="selected_rows_delete_button" type="button">
                            <i class="fa fa-trash"></i>
                            حذف العناصر المحددة
                        </button> <br>
                    </div>
                    <div class="table-responsive p-0">
                        <table class="table align-items-center mb-0 table-hover " id="questions_table" style="width:100%">
                            <thead>
                            <tr>
                                <th class="text-uppercase text-secondary text-s font-weight-bolder opacity-7">العنوان</th>
                                <th class="text-uppercase text-secondary text-s font-weight-bolder opacity-7">الوصف</th>
                                <th class="text-uppercase text-secondary text-s font-weight-bolder opacity-7 ps-2">القيمة</th>
                                <th class="text-uppercase text-secondary text-s font-weight-bolder opacity-7 ps-2">الصورة</th>
                                <th class="text-uppercase text-secondary text-s font-weight-bolder opacity-7 ps-2">الصوت</th>
                                <th class="text-uppercase text-secondary text-s font-weight-bolder opacity-7 ps-2">الفديو</th>
                                <th class="text-uppercase text-secondary text-s font-weight-bolder opacity-7">الإجراءات</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Toasts -->
    <div class="position-fixed top-0 start-0 p-lg-3" style="z-index: 11">
        <div class="toast fade hide p-2 mt-2 bg-gradient-info" role="alert" aria-live="assertive" id="infoToast" aria-atomic="true">
            <div class="toast-header bg-transparent border-0">
                <i class="material-icons text-white ms-2">notifications</i>
                <span class="ms-auto text-white font-weight-bold text-lg">إشعار</span>
                <i class="fas fa-times text-md text-white me-3 cursor-pointer" data-bs-dismiss="toast" aria-label="Close"></i>
            </div>
            <hr class="horizontal light m-0">
            <div class="toast-body text-white text-lg">
                جاري الحذف...
            </div>
        </div>

        <div class="toast fade hide p-2 bg-white" role="alert" aria-live="assertive" id="successToast" aria-atomic="true">
            <div class="toast-header border-0">
                <i class="material-icons text-success ms-2">check</i>
                <span class="ms-auto font-weight-bold text-lg">نجاح</span>
                <i class="fas fa-times text-md me-3 cursor-pointer" data-bs-dismiss="toast" aria-label="Close"></i>
            </div>
            <hr class="horizontal dark m-0">
            <div class="toast-body text-lg">
                تمت العملية بنجاح
            </div>
        </div>
    </div>
    <!-- End Toasts -->

    <script>
        $(document).ready(function () {
            $.get("{{ route('dashboard.questions.all') }}").done(function (data) {
                var table = $('#questions_table').DataTable({
                    pagingType: 'full_numbers',
                    retrieve: true,
                    pageLength: 5,
                    processing: true,
                    lengthMenu: [5, 10, 25, 50],
                    searching: true,
                    responsive: true,
                    "scrollX": true,
                    deferRender: true,
                    select: true,
                    select: {
                        style: 'multi'
                    },
                    "language": {
                        "lengthMenu": "عرض _MENU_ سطر في الصفحة",
                        "zeroRecords": "لم يتم ايجاد اي عنصر - عذراً",
                        "info": "عرض الصفحة _PAGE_ من _PAGES_",
                        "infoEmpty": "لا يوجد اي عنصر متطابق",
                        "infoFiltered": "(تم البحث في _MAX_  سطر)",
                        "loadingRecords": "يتم التحميل ...",
                        "processing": "تتم المعالجة ...",
                        "search": "بحث :",
                        "paginate": {
                            "first": "البداية",
                            "last": "النهاية",
                            "next": "التالي",
                            "previous": "السابق"
                        },
                    },

                    "data": data,
                    "order": [],
                    "columns": [
                        { "data": 'title' },
                        { "data": 'description' },
                        { "data": 'value' },
                        {
                            "data": "image",
                            "render": function (data, type, row) {
                                if (data) {
                                    // Construct the full image URL based on your application's logic
                                    var imageUrl = '/storage/media/' + data; // Replace with your actual image path logic
                                    // Create an <img> tag to display the image
                                    var imgTag = '<img src="' + imageUrl + '" alt="Image" style="max-width: 50px; max-height: 50px;">';
                                    // Create an <a> tag around the image to make it clickable
                                    var anchorTag = '<a href="' + imageUrl + '" target="_blank" title="View Image">' + imgTag + '</a>';

                                    // Return the clickable image
                                    return anchorTag;
                                }

                                // If data is not available, return an empty string
                                return '';
                            }
                        },
                        {
                            "data": "voice",
                            "render": function (data, type, row) {
                                if (data) {
                                    return '<i class="fa fa-volume-up"></i>';
                                }

                                return '';
                            }
                        },
                        {
                            "data": "video",
                            "render": function (data, type, row) {
                                if (data) {
                                    return '<i class="fa fa-video-camera"></i>';
                                }

                                // If data is not available, return an empty string
                                return '';
                            }
                        },
                        {
                            "data": "id",
                            "render": function (data, type, row) {
                                return `
                        <a href="/dashboard/questions/${data}/edit" class="btn btn-info" id='edit_question'><i class="fa fa-wrench"></i></a>
                           <a href="/dashboard/questions/${data}/answers" class="btn btn-success" id="add_answers"><i class="fa fa-plus-circle"></i> إضافة إجابات</a>
                        <button class="btn btn-danger" id='delete_question'><i class="fa fa-trash"></i></button>`;


                            },
                        },
                    ],

                });

                $('#selected_all_rows_button').on('click', function () {
                    table.rows({ page: 'current' }).select();
                });

                $('#deselected_all_rows_button').on('click', function () {
                    table.rows({ page: 'current' }).deselect();
                });

                $('#selected_rows_delete_button').on('click', function () {
                    var ids = table.rows({ selected: true }).data().map(row => {
                        return row.id;
                    }).toArray();
                    if (ids.length == 0) {
                        return false;
                    }
                    var infoDeleteingToast = document.getElementById('infoToast');
                    var infoToast = bootstrap.Toast.getInstance(infoDeleteingToast);
                    var successDeleteingToast = document.getElementById('successToast');
                    var successToast = bootstrap.Toast.getInstance(successDeleteingToast);

                    var rows_selected = table.rows('.selected');

                    Swal.fire({
                        title: 'هل أنت متأكد ؟',
                        text: `  سيتم حذف ${ids.length} عنصر ولا يمكنك التراجع `,
                        icon: 'warning',
                        showCancelButton: true,
                        cancelButtonText: 'الغاء',
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'نعم ، احذفه !'
                    }).then((result) => {
                        if (result.isConfirmed) {
                            infoToast.show();
                            $.ajax({
                                type: "DELETE",
                                headers: { 'X-CSRF-TOKEN': '{{ csrf_token() }}' },
                                url: "/dashboard/questions/" + ids,
                                contentType: false,
                                processData: false,
                                success: function (data) {
                                    console.log("success");
                                    console.log(data);
                                    rows_selected.remove().draw();
                                    infoToast.hide();
                                    successToast.show();
                                },
                                error: function (error) {
                                    console.log("error");
                                    console.log(error);
                                }
                            });
                        }
                    })
                });

                $('#questions_table tbody').on('click', '#delete_question', function () {
                    var infoDeleteingToast = document.getElementById('infoToast');
                    var infoToast = bootstrap.Toast.getInstance(infoDeleteingToast);
                    var successDeleteingToast = document.getElementById('successToast');
                    var successToast = bootstrap.Toast.getInstance(successDeleteingToast);

                    var data = table.row($(this).parents('tr')).data();
                    var row_clicked = table.row($(this).parents('tr'));

                    // if the table in responsive mode and the tr has a child
                    if (!data) {
                        var data = table.row($(this).closest('tr :not(.child)')).data();
                        var row_clicked = table.row($(this).closest('tr :not(.child)'));
                    }

                    Swal.fire({
                        title: 'هل أنت متأكد ؟',
                        text: "لا يمكنك التراجع عن ذلك !",
                        icon: 'warning',
                        showCancelButton: true,
                        cancelButtonText: 'الغاء',
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'نعم ، احذفه !'
                    }).then((result) => {
                        if (result.isConfirmed) {
                            infoToast.show();
                            $.ajax({
                                type: "DELETE",
                                headers: { 'X-CSRF-TOKEN': '{{ csrf_token() }}' },
                                url: "/dashboard/questions/" + data['id'],
                                contentType: false,
                                processData: false,
                                success: function (data) {
                                    console.log("success");
                                    console.log(data);
                                    row_clicked.remove().draw();
                                    infoToast.hide();
                                    successToast.show();
                                },
                                error: function (error) {
                                    console.log("error");
                                    console.log(error);
                                }
                            });
                        }
                    })
                });

                $('#questions_table tbody').on('click', '#edit_question', function () {
                    var data = table.row($(this).parents('tr')).data();

                    // if the table in responsive mode and the tr has a child
                    if (!data) {
                        var data = table.row($(this).closest('tr :not(.child)')).data();
                    }

                    window.location.href = "/dashboard/questions/" + data['id'] + "/edit";
                });
            });
        });
    </script>

@endsection
