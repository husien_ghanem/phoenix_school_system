@extends('admin.master')
@section('page-title', ' نتائج الامتحانات')
@section('navbar-title', ' نتائج الامتحانات')

@section('content')

    <div class="row">
        <div class="col-12">
            <div class="card my-4">
                <div class="card-header p-0 position-relative mt-n4 mx-3 z-index-2">
                    <div class="bg-gradient-primary shadow-primary border-radius-lg pt-4 pb-3">
                        <h6 class="text-white text-capitalize pe-3">نتائج الاختبارات السابقة   </h6>
                    </div>
                </div>
                <div class="card-body px-0 pb-2">
                    <div class="col-lg-12 col-md-12 px-2">
                        <div style="display: none" class="alert alert-danger alert-dismissible text-white" role="alert" id="alert_message">
                            <span class="text-sm"></span>
                            <ul class="text-sm m-0" id="errors_list"></ul>
                            <button type="button" class="btn-close text-lg py-3 opacity-10" data-bs-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <script>
                            $("#alert_message").on("close.bs.alert", function() {
                                // hide the alert >>> not remove it
                                $("#alert_message").hide();
                                return false;
                            });
                        </script>
                        <div class="row">
                            <div class="col-12 text-center my-3">
                                <h4>  </h4>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <h1 class="text-md-center m-4">نتائج الامتحان المتواجدة </h1>

        @forelse($classRecord as $class)
            <div class="mb-4 bg-gradient-primary shadow-primary border-radius-lg pt-4 pb-3 d-flex justify-content-between align-items-center flex-row-reverse px-8">
                <a href="{{ route('dashboard.view_class_results', ['class_id' => $class->id]) }}" class="btn btn-success">View results</a>
                <h2 class="d-xxl-inline-block"> اسم الصف : {{ $class->title }}</h2>
            </div>
        @empty
            <p class="text-center">لا توجد نتائج</p>
        @endforelse
    </div>


@endsection
