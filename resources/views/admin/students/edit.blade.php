@extends('admin.master')
@section('navbar-title', 'تعديل الطالب')

@section('content')
<div class="row">
    <div class=" col-12">
        <div class="card">
            <div class="card-header pb-0 px-3 ">
                <h6 class="mb-0 text-lg">تعديل الطالب </h6>
            </div>

            <div class="card-body pt-4 p-3">
                <div class="row gx-4 mb-2">

                    <div class="col-auto my-auto">
                      <div class="h-100">
                        <h5 class="mb-1">
                            {{$student->name}}
                        </h5>
                              طالب
                      </div>
                    </div>
                  </div>

                <div style="display: none" class="alert alert-danger alert-dismissible text-white" role="alert" id="alert_message">
                    <span class="text-sm">حصل خطأ اثناء تعديل بيانات الطالب</span>
                    <ul class="text-sm m-0" id="errors_list"></ul>
                    <button type="button" class="btn-close text-lg py-3 opacity-10" data-bs-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div style="display: none"  class="alert alert-success alert-dismissible text-white" role="alert" id="succcess_message">
                    <span class="text-sm">تم تعديل بيانات الطالب</span>
                    <button type="button" class="btn-close text-lg py-3 opacity-10" data-bs-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <script>
                    $("#alert_message,#succcess_message").on("close.bs.alert", function () {
                        // hide the alert >>> not remove it
                        $("#alert_message").hide();
                        $("#succcess_message").hide();
                        return false;
                    });
                </script>

                <form role="form" id="edit_student_form" class="text-end">
                    <div class="input-group input-group-outline my-3 is-filled">
                        <label class="form-label">الاسم</label>
                        <input id="name" type="text" class="form-control" value="{{$student->name}}">
                    </div>
                    <div class="input-group input-group-outline my-3 is-filled">
                      <label class="form-label">رقم الطالب</label>
                      <input id="student_number" type="number" class="form-control" value="{{$student->student_number}}">
                    </div>
                    <div class="input-group input-group-outline mb-3">
                      <label class="form-label">كلمة المرور</label>
                      <input id="password" type="password" class="form-control">
                    </div>
                    <div class="input-group input-group-outline my-3">


                        <select id="section_id" class="form-select" name="section_id">
                            <option value="" disabled selected>اختر القسم والصف</option>
                            @foreach ($sections as $section)
                                <option value="{{ $section->id }}" {{ $student->section_id == $section->id ? 'selected' : '' }}>
                                    {{ $section->title }} (الصف: {{ $section->class->title }})
                                </option>
                            @endforeach
                        </select>
                    </div>



                    <div class="text-center">
                      <button type="submit" class="btn bg-gradient-primary w-50 my-4 mb-2 text-lg">
                        <span id="button_submit_text">تعديل</span>
                        <div class="spinner" style="display: none;">
                          <div class="double-bounce1"></div>
                          <div class="double-bounce2"></div>
                        </div>
                      </button>
                    </div>
                  </form>
            </div>
        </div>
    </div>
</div>









<script>
    $('#edit_student_form').on('submit', function(e) {
       e.preventDefault();


       $("#button_submit_text").hide();
       $(".spinner").show();
       $("#alert_message").hide();
       $("#succcess_message").hide();

       var uploadData = new FormData();

       var name = $('#name').val();
       var student_number = $('#student_number').val();
       var password = $('#password').val();
        var section_id = $('#section_id').val();


       uploadData.append('name',name);
       uploadData.append('student_number',student_number);
       uploadData.append('password',password);
       uploadData.append('_method','PUT');
        uploadData.append('section_id',section_id);

       $.ajax({
           type: "POST",
           headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}'},
           url: '{{route('dashboard.students.update',$student)}}',
           data: uploadData,
           contentType: false,
           processData: false,
           statusCode: {
                422: function(error) {
                    // Only if your server returns a 422 status code can it come in this block. :-)
                    var errors = error.responseJSON.errors;
                    // console.log(errors);
                    var alert_message = document.getElementById("alert_message");
                    $("#alert_message").show();
                    var ul = document.getElementById("errors_list");
                    $('#errors_list').empty();
                    Object.entries(errors).forEach(function([key, value]){
                        var li = document.createElement("li");
                        li.appendChild(document.createTextNode(value));
                        ul.appendChild(li);
                    }
                    );
                },
                403: function() {
                    // Only if your server returns a 403 status code can it come in this block. :-)
                }
            },
           success:function(data){
                console.log("success");
                console.log(data);
                $("#button_submit_text").show();
                $(".spinner").hide();
                $("#succcess_message").show();
                window.location = window.location;
           },
           error:function(error){
                console.log("error");
                console.log(error);
                $("#button_submit_text").show();
                $(".spinner").hide();
                $("#alert_message").show();
                // myToast.show();
           }
       });
   });
</script>

@endsection
