@extends('admin.master')
@section('page-title', 'الاختبارات')
@section('navbar-title', 'الاختبارات')

@section('content')
<div class="row">
  <div class="col-12">
    <div class="card my-4">
      <div class="card-header p-0 position-relative mt-n4 mx-3 z-index-2">
        <div class="bg-gradient-primary shadow-primary border-radius-lg pt-4 pb-3">
          <h6 class="text-white text-capitalize pe-3">جدول الاختبارات المتوفرة</h6>
        </div>
      </div>
      <div class="card-body px-0 pb-2">

        <div class="col-lg-12 col-md-12 px-2">
          <div style="display: none" class="alert alert-danger alert-dismissible text-white" role="alert" id="alert_message">
            <span class="text-sm"></span>
            <ul class="text-sm m-0" id="errors_list"></ul>
            <button type="button" class="btn-close text-lg py-3 opacity-10" data-bs-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <script>
            $("#alert_message").on("close.bs.alert", function() {
              $("#alert_message").hide();
              return false;
            });
          </script>


            <form method="POST" class="text-end" enctype="multipart/form-data" action="{{ route('dashboard.tests.store') }}">
                @csrf
                <div class="row">
                    <div class="col-12 text-center my-3">
                        <h4>إضافة اختبار جديدة</h4>
                    </div>
                    <div class="col-12 col-md-12">
                        <label class="form-label">اسم الاختبار</label>
                        <div class="input-group input-group-outline my-2">
                            <input id="title" name="title" type="text" class="form-control">
                        </div>
                        @error('title')
                        <div class="text-danger">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="col-12 col-md-4">
                        <label class="form-label">الصف</label>
                        <select id="class_id" name="class_id" class="form-select">
                            <option value="" disabled selected>اختر الصف</option>
                            @foreach($classes as $class)
                                <option value="{{ $class->id }}">{{ $class->title }}</option>
                            @endforeach
                        </select>
                        @error('class_id')
                        <div class="text-danger">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="col-12 col-md-4">
                        <label class="form-label">عرض الاختبار</label>
                        <select id="shown" name="shown" class="form-select">
                            <option value="1">نعم</option>
                            <option value="0">لا</option>
                        </select>
                        @error('shown')
                        <div class="text-danger">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="col-12 col-md-4">
                        <label for="test_duration" class="form-label">مدة الاختبار (بالدقائق)</label>
                        <input type="number" class="form-control border" id="test_duration" name="test_duration" placeholder="ادخل مدة الاختبار">
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 my-3">
                        <button type="submit" class="btn bg-gradient-success w-100 mt-2 text-lg">
                            <span type="submit"><i class="fa fa-plus"></i> إضافة</span>
                            <div class="spinner" style="display: none;">
                                <div class="double-bounce1"></div>
                                <div class="double-bounce2"></div>
                            </div>
                        </button>
                    </div>
                </div>
            </form>



        </div>
        <hr />
        <h4 class="my-3 text-center">جدول الاختبارات المضافة</h4>
        <div class="table-responsive p-0">
          <table class="table align-items-center mb-0 table-hover " id="tests_table" style="width:100%">
            <thead>
              <tr>
                <th class="text-uppercase text-secondary text-s font-weight-bolder opacity-7 ps-2"> عنوان الاختبار </th>
                  <th class="text-uppercase text-secondary text-s font-weight-bolder opacity-7 ps-2">الصف </th>
                  <th class="text-uppercase text-secondary text-s font-weight-bolder opacity-7 ps-2"> ضمن العرض  </th>
                  <th class="text-uppercase text-secondary text-s font-weight-bolder opacity-7 ps-2"> الوقت المحدد بالدقائق </th>
                <th class="text-secondary text-s opacity-7">الاجراءات</th>
              </tr>
            </thead>
              <tbody>
              @foreach($tests as $test)
                  <td><a href="{{route('dashboard.tests.show',[$test->id] )}}">{{ $test->title }}</a></td>
                  <td>{{ $test->class->title }}</td>
                  <td>{{ $test->shown ? 'نعم' : 'لا' }}</td>
                  <td>{{ $test->test_duration ?? '0' }}</td>
                  <td>
                      <div class="d-flex">
                          @if (!$test->hasResults())
                          <form method="GET" action="{{ route('dashboard.tests.edit', [$test->id]) }}">
                              @csrf
                              <button type="submit" class="btn btn-info m-4">
                                  <i class="fa fa-wrench"></i>
                              </button>
                          </form>



                              <form method="POST" action="{{ route('dashboard.tests.destroy', [$test->id]) }}">
                                  @csrf
                                  @method('DELETE')
                                  <button type="submit" class="btn btn-danger delete-question m-4">
                                      <i class="fa fa-trash"></i>
                                  </button>
                              </form>
                          @else
                              <span>لا يمكن التعديل بسبب وجود نتائج لهذا الامتحان</span>
                          @endif
                      </div>

                  </td>


                  </tr>
              @endforeach
              </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection
