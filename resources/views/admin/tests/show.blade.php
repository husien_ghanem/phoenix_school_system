@extends('admin.master')

@section('page-title', 'عرض الاختبار')
@section('navbar-title', 'عرض الاختبار')

    <link href="{{ asset('admin/css/bootstrap.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('admin/css/bootstrap-select.css') }}" rel="stylesheet" />


@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card my-4">
                <div class="card-header p-0 position-relative mt-n4 mx-3 z-index-2">
                    <div class="bg-gradient-primary shadow-primary border-radius-lg pt-4 pb-3">
                        <h6 class="text-white text-capitalize pe-3"> تفاصيل الاختبار <span class="m-4">{{$test->title }}</span> </h6>
                    </div>
                </div>
                <div class="card-body px-0 pb-2">
                    <div class="col-lg-12 col-md-12 px-2">
                        <div class="alert alert-danger alert-dismissible text-white" role="alert" id="alert_message" style="display: none;">
                            <span class="text-sm"></span>
                            <ul class="text-sm m-0" id="errors_list"></ul>
                            <button type="button" class="btn-close text-lg py-3 opacity-10" data-bs-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        @if (!$test->hasResults())
                        <div class="text-center my-3">
                            <h4>إضافة أسئلة جديدة</h4>
                        </div>
                        <!-- Add Questions to Test Form -->

                        <div class="col-12">
                            <div class="row">
                                <div class="col-md-6">
                                    <form role="form" action="{{ route('dashboard.tests.add_question', ['id' => $test->id]) }}" method="post" id="add_questions_form" class="text-end" enctype="multipart/form-data">
                                        @csrf
                                        <div class="col-12">
                                            <label class="form-label">اختر الأسئلة من القائمة</label>
                                            <select name="selected_questions[]" id="selected_questions" class="selectpicker  form-control" multiple data-live-search="true">
                                                @foreach($questions as $question)
                                                    <option value="{{ $question->id }}">{{ $question->description }}</option>
                                                @endforeach
                                            </select>
                                            <script>
                                                $('select').selectpicker();
                                            </script>
                                        </div>
                                        <div class="col-12 my-3">
                                            <button type="submit" class="btn bg-gradient-success w-100 mt-2 text-lg">
                                                <span id="button_submit_text"><i class="fa fa-plus"></i> إضافة</span>
                                                <div class="spinner" style="display: none;">
                                                    <div class="double-bounce1"></div>
                                                    <div class="double-bounce2"></div>
                                                </div>
                                            </button>
                                        </div>
                                    </form>
                                </div>
                                <div class="col-md-6">
                                    <form role="form" action="{{ route('dashboard.tests.add_random_questions', ['test_id' => $test->id]) }}" method="post" id="add_random_questions_form" class="text-end" enctype="multipart/form-data">
                                        @csrf
                                        <div class="row">
                                            <div class="col-12 mb-3">
                                                <label class="form-label">عدد الأسئلة العشوائية</label>
                                                <select name="random_question_count" class="form-control" style="background-color: #F0F2F5;text-align: center;">
                                                    @for ($i = 1; $i <= $account_of_questions; $i++)
                                                        <option value="{{ $i }}">{{ $i }}</option>
                                                    @endfor
                                                </select>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-12 my-3">
                                                <button type="submit" class="btn bg-gradient-primary w-100 mt-2 text-lg" id="add_random_questions_btn">
                                                    <span><i class="fa fa-plus"></i> إضافة أسئلة عشوائية</span>
                                                </button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        @endif
                        <!-- Display Questions in Test -->
                        <hr />
                        <h4 class="my-3 text-center">الأسئلة في الاختبار</h4>
                        <div class="table-responsive p-0">
                            <table class="questions_table align-items-center mb-0 table-hover " id="questions_table" style="width:100%">
                                <thead>
                                <tr>
                                    <th class="text-uppercase text-secondary text-s font-weight-bolder opacity-7 ps-2">
                                        الأسئلة
                                    </th>
                                    <th class="text-secondary text-s opacity-7">الاجراءات</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($question_tests as $question_test)
                                    <td><span>{{ $question_test->description }}</span></td>
                                        <td>    @if (!$test->hasResults())
                                            <form method="post"
                                                  action="{{route('dashboard.tests.remove_question',[$test->id,$question_test->id] )}}">
                                                @csrf
                                                @method('DELETE')

                                                    <button type="submit" class="btn btn-danger delete-question ">
                                                        <i class="fa fa-trash"></i>
                                                    </button>

                                            </form>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Toasts -->
    <div class="position-fixed top-0 start-0 p-lg-3" style="z-index: 11">
        <!-- Info Toast -->
        <div class="toast fade hide p-2 mt-2 bg-gradient-info" role="alert" aria-live="assertive" id="infoToast" aria-atomic="true">
            <div class="toast-header bg-transparent border-0">
                <i class="material-icons text-white ms-2">notifications</i>
                <span class="ms-auto text-white font-weight-bold text-lg">اشعار</span>
                <i class="fas fa-times text-md text-white me-3 cursor-pointer" data-bs-dismiss="toast" aria-label="Close"></i>
            </div>
            <hr class="horizontal light m-0">
            <div class="toast-body text-white text-lg">
                يتم الآن الحذف ....
            </div>
        </div>
    </div>

    <!-- JavaScript -->

    <script  src="{{ asset('admin/js/bootstrap.bundle.min.js') }}"></script>
    <script  src="{{ asset('admin/js/bootstrap-select.min.js') }}"></script>
@endsection
<script  src="{{ asset('admin/js/jquery.min.js') }}"></script>
