@extends('admin.master')
@section('navbar-title', 'إضافة مدير')

@section('content')
<div class="row">
    <div class=" col-12">
        <div class="card">
            <div class="card-header pb-0 px-3 ">
                <h6 class="mb-0 text-lg">إضافة مدير جديد</h6>
            </div>
            
            <div class="card-body pt-4 p-3">

                <div style="display: none" class="alert alert-danger alert-dismissible text-white" role="alert" id="alert_message">
                    <span class="text-sm"></span>
                    <ul class="text-sm m-0" id="errors_list"></ul>
                    <button type="button" class="btn-close text-lg py-3 opacity-10" data-bs-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <script>
                    $("#alert_message").on("close.bs.alert", function () {
                        // hide the alert >>> not remove it
                        $("#alert_message").hide();
                        return false;
                    });
                </script>

                <form role="form" id="add_user_form" class="text-end">
                    <div class="input-group input-group-outline my-3">
                        <label class="form-label">الاسم</label>
                        <input id="name" type="text" class="form-control">
                    </div>
                    <div class="input-group input-group-outline my-3">
                      <label class="form-label">الإيميل</label>
                      <input id="email" type="email" class="form-control">
                    </div>
                    <div class="input-group input-group-outline mb-3">
                      <label class="form-label">كلمة المرور</label>
                      <input id="password" type="password" class="form-control">
                    </div>
                    
                    
                    

                    <div class="text-center">
                      <button type="submit" class="btn bg-gradient-primary w-50 my-4 mb-2 text-lg">
                        <span id="button_submit_text">إضافة</span>
                        <div class="spinner" style="display: none;">
                          <div class="double-bounce1"></div>
                          <div class="double-bounce2"></div>
                        </div>
                      </button>
                    </div>
                  </form>
            </div>
        </div>
    </div>
</div>






<script>
    $('#add_user_form').on('submit', function(e) {
       e.preventDefault();
 

       $("#button_submit_text").hide();
       $(".spinner").show();
       $("#alert_message").hide();

       var uploadData = new FormData();

       var name = $('#name').val();
       var email = $('#email').val();
       var password = $('#password').val();

       
       
       uploadData.append('name',name);
       uploadData.append('email',email);
       uploadData.append('password',password);
       uploadData.append('role',1);

       
       $.ajax({
           type: "POST",
           headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}'},
           url: '{{route('dashboard.users.store')}}',
           data: uploadData,
           contentType: false,
           processData: false,
           statusCode: {
                422: function(error) {
                    // Only if your server returns a 422 status code can it come in this block. :-)
                    var errors = error.responseJSON.errors;
                    // console.log(errors);
                    var alert_message = document.getElementById("alert_message");
                    $("#alert_message").show();
                    var ul = document.getElementById("errors_list");
                    $('#errors_list').empty();
                    Object.entries(errors).forEach(function([key, value]){
                        var li = document.createElement("li");
                        li.appendChild(document.createTextNode(value));
                        ul.appendChild(li);
                    }
                    );
                },
                403: function() {
                    // Only if your server returns a 403 status code can it come in this block. :-)
                }
            },
           success:function(data){
                console.log("success");
                console.log(data);
                $("#button_submit_text").show();
                $(".spinner").hide();
                window.location = '{{route('dashboard.users.index')}}';
           },
           error:function(error){
                console.log("error");
                console.log(error);
                $("#button_submit_text").show();
                $(".spinner").hide();
                // myToast.show();
           }
       });
   });
</script>



@endsection