@extends('admin.master')
@section('page-title', 'نتائج الاختبار')
@section('navbar-title', 'نتائج الاختبار')

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card my-4">
                <div class="card-header p-0 position-relative mt-n4 mx-3 z-index-2">
                    <div class="bg-gradient-primary shadow-primary border-radius-lg pt-4 pb-3">
                        <h6 class="text-white text-capitalize pe-3">نتائج الاختبارات السابقة</h6>
                    </div>
                </div>
                <div class="card-body px-0 pb-2">
                    <div class="col-lg-12 col-md-12 px-2">
                        <div style="display: none" class="alert alert-danger alert-dismissible text-white" role="alert" id="alert_message">
                            <span class="text-sm"></span>
                            <ul class="text-sm m-0" id="errors_list"></ul>
                            <button type="button" class="btn-close text-lg py-3 opacity-10" data-bs-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <script>
                            $("#alert_message").on("close.bs.alert", function() {
                                // hide the alert >>> not remove it
                                $("#alert_message").hide();
                                return false;
                            });
                        </script>
                        <div class="row">
                            <div class="col-12 text-center my-3">
                                <h4>نتائج الاختبارات للطالب: {{ $results->name }}</h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-12">
            <div class="card my-4">
                <div class="card-header p-0 position-relative mt-n4 mx-3 z-index-2">
                </div>
                <div class="card-body px-0 pb-2">
                    <div class="table-responsive p-0">
                        <table class="table align-items-center mb-0 table-hover" id="students_table" style="width:100%">
                            <thead>
                            <tr>
                                <th class="text-uppercase text-secondary text-s font-weight-bolder opacity-7">#</th>
                                <th class="text-uppercase text-secondary text-s font-weight-bolder opacity-7 ps-2">عنوان الاختبار</th>
                                <th class="text-uppercase text-secondary text-s font-weight-bolder opacity-7 ps-2">النسبة المحققة</th>
                                <th class="text-secondary text-s opacity-7">الاجراءات</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($results->results as $index => $result)
                                <tr>
                                    <td>{{ $index + 1 }}</td>
                                    <td>{{ optional($result->test)->title ?? 'N/A' }}</td>
                                    <td>{{ $result->result_percent }}%</td>
                                    <td>
                                        <a href="{{ route('student.show.test.result.admin', ['result' => $result->id,]) }}" class="btn btn-primary"><i class="fa fa-eye"></i> عرض الاختبار</a>
                                        <form action="{{ route('student.test.result.delete', ['id' => $result->id]) }}" method="POST" style="display: inline;">
                                            @csrf
                                            @method('DELETE')
                                            <button type="submit" class="btn btn-danger">حذف نتيجة الامتحان</button>
                                        </form>
                                    </td>

                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
