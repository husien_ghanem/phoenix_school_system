@extends('admin.front-master')

@section('page-title', 'الاختبار')
@section('navbar-title', 'الختبار ..')

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card my-4">
                <div class="card-header p-0 position-relative mt-n4 mx-3 z-index-2">
                    <div class="bg-gradient-primary shadow-primary border-radius-lg pt-4 pb-3">
                        <p class="text-white text-capitalize pe-3">اهلا بك  {{ auth('student')->user()->name }}</p>
                        <h6 class="text-white text-capitalize pe-3">جدول الاختبارات المتوفرة </h6>
                    </div>
                </div>
                <div class="card-body px-0 pb-2">
                    <div class="col-lg-12 col-md-12 px-2">
                        <div style="display: none" class="alert alert-danger alert-dismissible text-white" role="alert" id="alert_message">
                            <span class="text-sm"></span>
                            <ul class="text-sm m-0" id="errors_list"></ul>
                            <button type="button" class="btn-close text-lg py-3 opacity-10" data-bs-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <script>
                            $("#alert_message").on("close.bs.alert", function() {
                                // hide the alert >>> not remove it
                                $("#alert_message").hide();
                                return false;
                            });
                        </script>
                        <div class="row">
                            @if ($tests->isEmpty())
                                <div class="col-12 text-center my-3">
                                    <h4> لا توجد اختبارات متاحة حاليًا</h4>
                                </div>
                            @else
                            <div class="col-12 text-center my-3">
                                <h4> اختر الاختبار المطلوب </h4>
                            </div>
                            @endif
                        </div>
                    </div>
                    <hr />
                </div>
            </div>
        </div>
    </div>
    <div class="container">

        @if ($tests->isEmpty())

        @else
            <h4 class="mt-4">الاختبارات المتوفرة:</h4>
            <form method="POST" action="{{ route('student.show.test') }}">
                @csrf
                <ul class="list-group">
                    @foreach ($tests as $test)
                        @if ($test->shown)
                            <li class="list-group-item">
                                <label>
                                    <input type="radio" name="selected_test" value="{{ $test->id }}">
                                    {{ $test->title }}
                                </label>
                            </li>
                        @endif
                    @endforeach
                </ul>
                <button type="submit" class="btn btn-primary mt-3">بدء الاختبار</button>
            </form>
        @endif
    </div>

@endsection
