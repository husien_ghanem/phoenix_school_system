@extends('admin.front-master')

@section('page-title', 'الاختبار')
@section('navbar-title', 'الختبار ..')

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card my-4">
                <div class="card-header p-0 position-relative mt-n4 mx-3 z-index-2">
                    <div class="bg-gradient-primary shadow-primary border-radius-lg pt-4 pb-3">
                        <p class="text-white text-capitalize pe-3">اهلا بك  {{ auth('student')->user()->name }}</p>
                        <h6 class="text-white text-capitalize pe-3">نتائج الاختبار  </h6>
                    </div>
                </div>
                <div class="card-body px-0 pb-2">
                    <div class="col-lg-12 col-md-12 px-2">
                        <div style="display: none" class="alert alert-danger alert-dismissible text-white" role="alert" id="alert_message">
                            <span class="text-sm"></span>
                            <ul class="text-sm m-0" id="errors_list"></ul>
                            <button type="button" class="btn-close text-lg py-3 opacity-10" data-bs-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <script>
                            $("#alert_message").on("close.bs.alert", function() {
                                // hide the alert >>> not remove it
                                $("#alert_message").hide();
                                return false;
                            });
                        </script>
                        <div class="row">
                            <div class="col-12 text-center my-3">
                                <h4>  </h4>
                            </div>
                        </div>
                    </div>
                    <hr />
                </div>
            </div>
        </div>
    </div>

        <div class="container">


            <div class="alert alert-success">
                <h6 class="text-white text-capitalize pe-3">نتائج الاختبار  </h6>

                <p class="text-white text-capitalize pe-3">إجمالي قيمة الأسئلة : {{ $totalQuestionsValue }}</p>
                <p class="text-white text-capitalize pe-3">إجمالي قيمة الإجابات الصحيحة : {{ $totalCorrectAnswersValue }}</p>
                <p class="text-white text-capitalize pe-3">النسبة المؤية : {{ number_format($percentageCorrect, 2) }} %</p>

            </div>


        </div>
    @endsection

