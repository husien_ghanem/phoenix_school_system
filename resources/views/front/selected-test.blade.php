@extends('admin.front-master')

@section('page-title', 'الاختبار')
@section('navbar-title', 'الختبار ..')

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card my-4">
                <div class="card-header p-0 position-relative mt-n4 mx-3 z-index-2">
                    <div class="bg-gradient-primary shadow-primary border-radius-lg pt-4 pb-3">
                        <p class="text-white text-capitalize pe-3">اهلا بك  {{ auth('student')->user()->name }}</p>
                        <h6 class="text-white text-capitalize pe-3">
                            @if ($test->test_duration !== null)
                                مدة الاختبار هي {{ $test->test_duration }} دقيقة
                            @else
                                مدة الاختبار غير محددة
                            @endif
                        </h6>
                    </div>
                </div>
                <div class="card-body px-0 pb-2">
                    <div class="col-lg-12 col-md-12 px-2">
                        <div style="display: none" class="alert alert-danger alert-dismissible text-white" role="alert" id="alert_message">
                            <span class="text-sm"></span>
                            <ul class="text-sm m-0" id="errors_list"></ul>
                            <button type="button" class="btn-close text-lg py-3 opacity-10" data-bs-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <script>
                            $("#alert_message").on("close.bs.alert", function() {
                                // hide the alert >>> not remove it
                                $("#alert_message").hide();
                                return false;
                            });
                        </script>
                        <div class="row">
                            <div class="col-12 text-center my-3">
                                <h4> اختر الإجابة الصحيحة </h4>
                            </div>
                        </div>
                    </div>
                    <hr />
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <h1>{{ $test->title }}</h1>
        <form method="post" action="{{ route('student.submit.test', ['test_id' => $test->id]) }}" id="test-form">
            @csrf
            @foreach ($test->questions as $question)
                <div class="card mb-3">
                    <div class="card-body">
                        <h5 class="card-title">
                            {{ $question->title }}<span class="ml-6"><hr></span>(الدرجة : {{ $question->value }})
                        </h5>
                        <p class="card-text">{{ $question->description }}</p>
                        <div class="row">
                            @if ($question->image)
                                <div class="col-md-4">
                                    <a href="{{ asset('storage/media/'.$question->image) }}">
                                        <img src="{{ asset('storage/media/'.$question->image) }} " alt="Question Image" class="img-fluid">
                                    </a>
                                </div>
                            @endif
                            @if ($question->voice)
                                <div class="col-md-4">
                                    <audio controls>
                                        <source src="{{ asset('storage/media/'.$question->voice) }}" type="audio/mpeg">
                                        Your browser does not support the audio element.
                                    </audio>
                                </div>
                            @endif
                            @if ($question->video)
                                <div class="col-md-4">
                                    <video width="320" height="240" controls>
                                        <source src="{{ asset('storage/media/'.$question->video) }}" type="video/mp4">
                                        Your browser does not support the video tag.
                                    </video>
                                </div>
                            @endif
                        </div>
                        <div class="row m-4">
                            @foreach ($question->answers as $answer)
                                <div class="col-md-3 mb-2"> <!-- Adjust the column size as needed -->
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="{{ "$question->id" }}" id="answer_{{ $answer->id }}" value={{ $answer->id }}>
                                        <label class="form-check-label" for="answer_{{ $answer->id }}">
                                            {{ $answer->answer_text }}
                                        </label>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            @endforeach
            <button type="submit" class="btn btn-primary" id="submit-button">Submit Answers</button>
        </form>
    </div>
    <div id="timer">Time remaining: <span id="time-display">00:00</span></div>



    <script>
        // Set the duration of the test in seconds
        const testDuration = {{ $test->test_duration }} * 60;

        // Function to start the timer
        function startTimer() {
            // Check if the test has been completed
            let testCompleted = '{{ session('test_completed') }}';

            if (testCompleted) {
                // If completed, redirect to the results page or any other appropriate page
                window.location.href = '{{ route('student.test.result') }}';
                return;
            }
            // Calculate the end time based on the current time and test duration
            const currentTime = new Date().getTime();
            const endTime = currentTime + (testDuration * 1000);

            // Store the end time in session storage
            sessionStorage.setItem('test_end_time', endTime);

            // Start updating the timer display
            updateTimerDisplay();
        }

        // Function to update the timer display
        function updateTimerDisplay() {


            // Get the end time from session storage
            const endTime = sessionStorage.getItem('test_end_time');

            if (!endTime) {
                // If end time is not set, start the timer
                startTimer();
                return;
            }

            // Calculate the remaining time based on the current time
            const currentTime = new Date().getTime();
            const remainingTime = endTime - currentTime;

            if (remainingTime <= 0) {
                // If time is up, submit the test
                submitTest();
            } else {
                // Display the remaining time
                const minutes = Math.floor((remainingTime / 1000) / 60).toString().padStart(2, '0');
                const seconds = Math.floor((remainingTime / 1000) % 60).toString().padStart(2, '0');
                document.getElementById('time-display').textContent = `${minutes}:${seconds}`;
            }
        }

        // Update the timer display every second
        setInterval(updateTimerDisplay, 1000);

        // Function to submit the test
        function submitTest() {
            // Automatically click the "Submit Answers" button
            const submitButton = document.getElementById('submit-button');
            if (submitButton) {
                submitButton.click();
            }
        }

        // Start the timer when the page loads
        startTimer();
    </script>
@endsection




