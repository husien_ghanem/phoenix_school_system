@extends('admin.master')
@section('page-title', 'نتائج الاختبار')
@section('navbar-title', 'نتائج الاختبار')

@section('content')

    <div class="row">
        <div class="col-12">
            <div class="card my-4">
                <div class="card-header p-0 position-relative mt-n4 mx-3 z-index-2">
                    <div class="bg-gradient-primary shadow-primary border-radius-lg pt-4 pb-3">

                        <h6 class="text-white text-capitalize pe-3"> نتائج اختبار {{ $result->test->title }}  </h6>
                    </div>
                </div>
                <div class="card-body px-0 pb-2">
                    <div class="col-lg-12 col-md-12 px-2">
                        <div style="display: none" class="alert alert-danger alert-dismissible text-white" role="alert"
                             id="alert_message">
                            <span class="text-sm"></span>
                            <ul class="text-sm m-0" id="errors_list"></ul>
                            <button type="button" class="btn-close text-lg py-3 opacity-10" data-bs-dismiss="alert"
                                    aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <script>
                            $("#alert_message").on("close.bs.alert", function () {
                                // hide the alert >>> not remove it
                                $("#alert_message").hide();
                                return false;
                            });
                        </script>
                        <div class="row">
                            <div class="col-12 text-center my-3">
                                <h4></h4>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="bg-gradient-primary shadow-primary border-radius-lg pt-4 pb-3">
            <h1>نتائج الاختبار :</h1>
            <p style="color: #f9f9f9; margin-right: 20px;">اسم الطالب : {{ $result->student->name }}</p>
            <p style="color: #f9f9f9;margin-right: 20px;">اسم الاختبار: {{ $result->test->title }}</p>
            <p style="color: #f9f9f9;margin-right: 20px;">العلامة : {{ $result->result_percent }}%</p>
        </div>
        <br>   <br>
        <ul>
            @foreach($questions as $index => $question)
                <div class="bg-gradient-primary shadow-primary border-radius-lg pt-4 pb-3">
                    <li  style="color: #f9f9f9; margin-right: 20px;">
                        السؤال {{ $index + 1 }} :  {{ $question->description }}
                        <br>
                        اجابة الطالب    :    {{ $answers[$index]->answer_text }}
                    </li>
                </div>
                <br>
            @endforeach
        </ul>
    </div>
@endsection
