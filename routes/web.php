<?php

use App\Http\Controllers\admin\{AnswersController,
    AuthController,
    ClassesController,
    GeneralController,
    QuestionController,
    ResultController,
    SectionsController,
    StudentController,
    TestController,
    UserController};
use App\Http\Controllers\front\FrontController;
use App\Http\Controllers\WordParserController;
use Illuminate\Support\Facades\Route;


Route::get('/', function () {
    return view('welcome');
});

// Auth admin
Route::get('/login-admin', function () { return view('admin.login');})->name('admin.login');
Route::get('/logout', [AuthController::class, 'logout'])->name('logout');
Route::post('/authenticate-admin', [AuthController::class, 'authenticate_admin'])->name('admin.authenticate');

// Auth student
Route::get('/login-student', function () { return view('admin.students.login');})->name('student.login');
Route::post('/authenticate-student', [\App\Http\Controllers\student\AuthController::class, 'authenticate_student'])->name('student.authenticate');


//Dashboard
Route::group([
    'prefix' => '/dashboard',
    'middleware' => ['admin'],
    'as' => 'dashboard.'
],
    function () {
    Route::get('/', [GeneralController::class, 'dashboard_index'])->name('index');
    //datatable
    Route::get('users/all', [UserController::class, 'all_users'])->name('users.all');
    Route::get('classes/all', [ClassesController::class, 'all_classes'])->name('classes.all');
    Route::get('sections/{id}/get_all', [SectionsController::class, 'all_sections'])->name('sections.all');
    Route::get('questions/all', [QuestionController::class, 'all_questions'])->name('questions.all');
    Route::get('students/all', [StudentController::class, 'all_students'])->name('students.all');
    Route::get('questions/{question}/answers', [AnswersController::class, 'addAnswers'])->name('questions.answers');

    Route::get('tests/all', [TestController::class, 'all_tests'])->name('tests.all');
    Route::get('questions/{id}/get_all', [QuestionController::class, 'all_questions_test'])->name('questions.test');
    Route::delete('tests/{test_id}/remove_question/{question_id}', [TestController::class, 'remove_question'])->name('tests.remove_question');
    Route::post('tests/{test_id}/add_random_questions', [QuestionController::class, 'add_random_questions'])->name('tests.add_random_questions');
    Route::post('tests/{id}/add_question', [TestController::class, 'add_question'])->name('tests.add_question');
    Route::get('student/submitted-exam-results', [ResultController::class, 'submittedExamResults'])->name('submitted_exam_results');
    Route::get('admin/view-class-results', [ResultController::class, 'viewClassResults'])->name('view_class_results');
    Route::get('results/class/all/{class_id}', [ResultController::class, 'getViewClassResults'])->name('view_class_results.all');
    Route::get('admin/view/previous-tests/{result}', [ResultController::class, 'previousTests'])->name('previous.tests.view');
    Route::post('/questions/upload', [WordParserController::class, 'upload'])->name('questions.upload');


        //resource
    Route::resources([
        'users' => UserController::class,
    ]);
    Route::resources([
        'classes' => ClassesController::class,
    ]);
    Route::resources([
        'sections' => SectionsController::class,
    ]);
    Route::resources([
        'questions' => QuestionController::class,
    ]);
    Route::resources([
        'students' =>StudentController::class,
    ]);
    Route::resources([
        'tests' => TestController::class,
    ]);
    Route::resources([
        'answers' =>AnswersController::class,
    ]);
});



Route::group([
    'prefix' => '/student',
    'middleware' => ['student'],
    'as' => 'student.'
           ],function (){

    Route::post('test/show/',  [FrontController::class, 'showTest'])->name('show.test')->middleware('checkTestCompletion');
    Route::get('available-tests', [FrontController::class, 'availableTests'])->name('available.tests');

    Route::post('tests/submit/{test_id}', [FrontController::class, 'submitTest'])->name('submit.test')->middleware('checkTestCompletion');
    Route::get('/results', [FrontController::class, 'showResults'])->name('test.result');

});

Route::get('student/all-results/{student_id}', [ResultController::class, 'allResults'])->name('student.all-results-students');

Route::get('student/all-results', [ResultController::class, 'index'])->name('student.all-results');

// View Test Results
Route::get('/student/test/result/view/{result}', [ResultController::class, 'viewTestResults'])->name('student.test.result.view');
Route::get('/student/show/testResult/view/{result}', [ResultController::class, 'viewTestResultsForAdmin'])->name('student.show.test.result.admin');

// Delete Test Results
Route::delete('/student/test/result/delete/{id}', [ResultController::class, 'deleteTestResults'])->name('student.test.result.delete');
//
